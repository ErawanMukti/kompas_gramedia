-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Okt 2017 pada 22.45
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbskripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `areas`
--

INSERT INTO `areas` (`id`, `lokasi`, `created_at`, `updated_at`) VALUES
(1, 'Surabaya', '2017-08-03 23:55:38', '2017-08-03 23:55:38'),
(2, 'Jakarta', '2017-08-03 23:55:49', '2017-08-03 23:55:49'),
(3, 'Yogyakarta', '2017-08-03 23:56:10', '2017-08-03 23:56:10'),
(6, 'Sidoarjo Kota', '2017-09-29 22:36:01', '2017-09-29 15:36:01'),
(7, 'Bandung', '2017-09-29 15:36:25', '2017-09-29 15:36:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gedungs`
--

CREATE TABLE `gedungs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_area` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `gedungs`
--

INSERT INTO `gedungs` (`id`, `nama_gedung`, `alamat_gedung`, `no_telp_gedung`, `id_area`, `created_at`, `updated_at`) VALUES
(1, 'KG Jemursari', 'Jl Raya Jemursari 1', '03112341234', 1, '2017-08-04 07:01:20', '2017-08-04 07:01:20'),
(2, 'KG Palmerah', 'Jl Palmerah Barat 1 - 12', '02112341231', 2, '2017-08-04 14:02:00', '2017-08-04 07:02:00'),
(3, 'KG Yos Surdarso', 'Jl Yos Sudarso 21', '0234123432100000000000000', 3, '2017-08-31 14:15:23', '2017-08-31 07:15:23'),
(4, 'Bumi Mandiri', 'Sidoarjo', '12345678', 6, '2017-09-29 15:51:12', '2017-09-29 15:51:12'),
(5, 'Surabaya Square', 'Surabaya', '12345678', 1, '2017-09-29 23:03:27', '2017-09-29 16:03:27'),
(6, 'Co Worker Space', 'Bandung', '12345678', 7, '2017-09-29 16:04:07', '2017-09-29 16:04:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `isis`
--

CREATE TABLE `isis` (
  `id` int(10) UNSIGNED NOT NULL,
  `jawaban` int(11) NOT NULL,
  `id_kontrak` int(10) UNSIGNED NOT NULL,
  `id_pertanyaan` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `isis`
--

INSERT INTO `isis` (`id`, `jawaban`, `id_kontrak`, `id_pertanyaan`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 2, '2017-10-01', '2017-09-30 19:48:20', '2017-09-30 19:48:20'),
(2, 2, 6, 3, '2017-10-01', '2017-09-30 19:48:20', '2017-09-30 19:48:20'),
(3, 3, 6, 4, '2017-10-01', '2017-09-30 19:48:20', '2017-09-30 19:48:20'),
(4, 4, 6, 5, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(5, 5, 6, 6, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(6, 1, 6, 7, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(7, 2, 6, 8, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(8, 3, 6, 9, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(9, 4, 6, 10, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(10, 5, 6, 11, '2017-10-01', '2017-09-30 19:48:21', '2017-09-30 19:48:21'),
(11, 1, 6, 2, '2017-11-01', '2017-09-30 19:54:30', '2017-09-30 19:54:30'),
(12, 1, 6, 3, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(13, 1, 6, 4, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(14, 1, 6, 5, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(15, 1, 6, 6, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(16, 1, 6, 7, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(17, 1, 6, 8, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(18, 1, 6, 9, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(19, 1, 6, 10, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(20, 1, 6, 11, '2017-11-01', '2017-09-30 19:54:31', '2017-09-30 19:54:31'),
(21, 1, 6, 2, '2018-10-01', '2017-09-30 20:00:21', '2017-09-30 20:00:21'),
(22, 1, 6, 3, '2018-10-01', '2017-09-30 20:00:21', '2017-09-30 20:00:21'),
(23, 1, 6, 4, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(24, 1, 6, 5, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(25, 1, 6, 6, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(26, 1, 6, 7, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(27, 1, 6, 8, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(28, 1, 6, 9, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(29, 1, 6, 10, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22'),
(30, 1, 6, 11, '2018-10-01', '2017-09-30 20:00:22', '2017-09-30 20:00:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komplains`
--

CREATE TABLE `komplains` (
  `id` int(10) UNSIGNED NOT NULL,
  `subjek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi_komplain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_komplain` datetime NOT NULL,
  `id_users` int(10) UNSIGNED DEFAULT NULL,
  `id_kontrak` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0: Pending, 1:Selesai',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `komplains`
--

INSERT INTO `komplains` (`id`, `subjek`, `isi_komplain`, `tanggal_komplain`, `id_users`, `id_kontrak`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AC Tidak Dingin', 'Mohon untuk diperiksa ac ruangan arjuna karena tidak dingin', '2017-09-30 00:00:00', 13, 6, 1, '2017-09-30 08:14:01', '2017-09-30 01:14:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komplain_balas`
--

CREATE TABLE `komplain_balas` (
  `id` int(11) NOT NULL,
  `id_komplain` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `tgl_balas` date NOT NULL,
  `pesan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komplain_balas`
--

INSERT INTO `komplain_balas` (`id`, `id_komplain`, `id_users`, `tgl_balas`, `pesan`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '2017-09-30', 'Komplain akan kami tangani dalam 2x24 jam', '2017-09-30 01:13:53', '2017-09-30 01:13:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontraks`
--

CREATE TABLE `kontraks` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_kontrak` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `tanggal_masuk` datetime NOT NULL,
  `tanggal_keluar` datetime NOT NULL,
  `file_kontrak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_users` int(10) UNSIGNED NOT NULL,
  `id_ruangan` int(10) UNSIGNED NOT NULL,
  `id_perjanjian` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kontraks`
--

INSERT INTO `kontraks` (`id`, `no_kontrak`, `jangka_waktu`, `tanggal_masuk`, `tanggal_keluar`, `file_kontrak`, `id_users`, `id_ruangan`, `id_perjanjian`, `created_at`, `updated_at`) VALUES
(2, 'CON-1708-001', 5, '2017-08-05 00:00:00', '2022-05-04 00:00:00', 'copi.docx', 3, 1, 1, '2017-10-01 08:11:02', '2017-08-13 10:40:18'),
(4, 'CON-1709-001', 6, '2017-09-28 00:00:00', '2017-09-29 00:00:00', 'client.txt', 3, 1, 1, '2017-10-01 08:11:12', '2017-09-23 12:21:19'),
(6, 'CON-1709-002', 1, '2017-01-01 00:00:00', '2017-01-01 00:00:00', 'python_nltk_docs.pdf', 13, 5, 2, '2017-10-01 08:11:24', '2017-09-29 22:04:27'),
(7, 'CON-1710-001', 1, '2017-01-01 00:00:00', '2017-01-01 00:00:00', 'Resume (EN Version).pdf', 3, 3, 1, '2017-10-01 08:17:00', '2017-10-01 01:17:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuisioner_kepuasans`
--

CREATE TABLE `kuisioner_kepuasans` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_section` int(11) NOT NULL,
  `pertanyaan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kuisioner_kepuasans`
--

INSERT INTO `kuisioner_kepuasans` (`id`, `id_section`, `pertanyaan`, `created_at`, `updated_at`) VALUES
(2, 4, 'Sampah Selalu Dibersihkan Setiap Hari', '2017-09-30 18:53:00', '2017-09-30 18:53:00'),
(3, 4, 'Meja Selalu Dibersihkan Setiap Hari', '2017-09-30 18:56:12', '2017-09-30 18:56:12'),
(4, 4, 'Ruangan Selalu Dibersihkan Setiap Hari', '2017-09-30 18:56:31', '2017-09-30 18:56:31'),
(5, 5, 'Setiap Tamu Diwajibkan Melapor Security', '2017-09-30 18:56:53', '2017-09-30 18:56:53'),
(6, 5, 'CCTV Berfungsi 24 Jam', '2017-09-30 18:57:08', '2017-09-30 18:57:08'),
(7, 6, 'Taman Selalu Disiram Setiap Hari', '2017-09-30 18:57:24', '2017-09-30 18:57:24'),
(8, 6, 'Sampah Daun Selalu Dibersihkan Setiap Hari', '2017-10-01 02:02:31', '2017-09-30 19:02:31'),
(9, 7, 'Staff Selalu Menyapa Setiap Berpapasan', '2017-09-30 19:02:58', '2017-09-30 19:02:58'),
(10, 7, 'Staff Selalu Tersenyum Ketika Menyapa', '2017-09-30 19:03:21', '2017-09-30 19:03:21'),
(11, 7, 'Staff Menggunakan Kata Yang Sopan Ketika Berbicara', '2017-10-01 02:03:55', '2017-09-30 19:03:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lihats`
--

CREATE TABLE `lihats` (
  `id` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_survei` int(10) UNSIGNED NOT NULL,
  `id_ruangan` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemens`
--

CREATE TABLE `manajemens` (
  `id` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_users` int(10) UNSIGNED NOT NULL,
  `id_ruangan` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_05_05_092130_create_area_table', 1),
('2017_05_05_092138_create_gedung_table', 1),
('2017_05_05_092144_create_ruangan_table', 1),
('2017_05_05_092458_create_perjanjian_table', 1),
('2017_05_05_092673_create_kontrak_table', 1),
('2017_05_05_093825_create_transaksi_table', 1),
('2017_05_05_094138_create_komplain_table', 1),
('2017_05_05_094139_create_section_table', 1),
('2017_05_05_094200_create_kuisioner_kepuasan_table', 1),
('2017_05_05_094349_create_survei_table', 1),
('2017_05_05_094423_create_manajemen_table', 1),
('2017_05_05_094530_create_lihat_table', 1),
('2017_05_05_094545_create_isi_table', 1),
('2017_09_17_163419_buat_tabel_laporanpermintaan', 2),
('2017_09_17_163419_create_permintaan_table', 3),
('2017_09_19_161719_create_permintaan_table', 4),
('2017_09_19_162610_create_permintaan_table', 5),
('2017_09_19_181711_create_permintaan_table', 6),
('2017_09_27_005547_create_saran_table', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perjanjians`
--

CREATE TABLE `perjanjians` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_perjanjian` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `nama_perjanjian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi_pasal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `perjanjians`
--

INSERT INTO `perjanjians` (`id`, `no_perjanjian`, `nama_perjanjian`, `isi_pasal`, `created_at`, `updated_at`) VALUES
(1, 'PJJ-1710-001', 'Perjanjian 123', 'tambahan.txt', '2017-10-01 07:58:37', '2017-09-29 20:44:01'),
(2, 'PJJ-1710-002', 'Perjanjian 2', 'python_nltk_docs.pdf', '2017-10-01 07:58:40', '2017-09-29 20:34:18'),
(3, 'PJJ-1710-003', 'Perjanjian 1', 'Resume (EN Version).pdf', '2017-10-01 08:03:12', '2017-10-01 01:03:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permintaan`
--

CREATE TABLE `permintaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_permintaan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isi_permintaan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_pengajuan` datetime NOT NULL,
  `id_users` int(10) UNSIGNED DEFAULT NULL,
  `id_transaksi` int(10) UNSIGNED DEFAULT NULL,
  `id_kontrak` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0: Pending, 1:Approve, 2:Reject',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `permintaan`
--

INSERT INTO `permintaan` (`id`, `nama_permintaan`, `isi_permintaan`, `tanggal_pengajuan`, `id_users`, `id_transaksi`, `id_kontrak`, `status`, `created_at`, `updated_at`) VALUES
(19, 'Tambah Line Telepon', 'python_nltk_docs.pdf', '2017-09-30 00:00:00', 13, NULL, 6, 1, '2017-10-01 20:32:43', '2017-10-01 13:32:43'),
(20, 'Tambah Line Telepon', NULL, '2017-09-30 00:00:00', 13, NULL, 6, 0, '2017-10-01 20:22:39', '2017-09-29 23:10:05'),
(24, 'Perpanjangan Sewa', 'isi permintaan', '2017-10-01 00:00:00', 13, NULL, 6, 0, '2017-10-01 13:39:34', '2017-10-01 13:39:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangans`
--

CREATE TABLE `ruangans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fasilitas_umum` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fasilitas_dalam_ruangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_ruangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deposit` double NOT NULL,
  `harga_perm` double NOT NULL,
  `service_charge` double NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `luas_ruangan` double NOT NULL,
  `biaya_overtime_ac` double DEFAULT NULL,
  `pajak_pph` double NOT NULL,
  `id_gedung` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `ruangans`
--

INSERT INTO `ruangans` (`id`, `nama`, `fasilitas_umum`, `fasilitas_dalam_ruangan`, `jenis_ruangan`, `deposit`, `harga_perm`, `service_charge`, `foto`, `luas_ruangan`, `biaya_overtime_ac`, `pajak_pph`, `id_gedung`, `created_at`, `updated_at`) VALUES
(1, 'Jemursari Timur 1', 'Parkiran', 'Pantry', 'standar', 100000, 100000, 10000, '9c7e509b3a3c8c3141928a552c7b410a.jpg', 12, 100000, 0, 1, '2017-09-30 01:52:58', '2017-09-29 18:52:58'),
(3, 'Ruangan Kantor A', 'Parkiran, ATM, Security', 'Meeting Room, Interior Minimalis', 'standar', 100000, 100000, 10000, '1.jpg', 12, 100000, 0, 1, '2017-08-13 04:25:19', '2017-08-12 21:25:19'),
(4, 'Ruangan Kantor B', 'Parkiran, ATM, Security', 'Meeting Room, Interior Minimalis', 'standar', 100000, 100000, 10000, '2.jpg', 12, 100000, 0, 1, '2017-08-13 04:25:19', '2017-08-12 21:25:19'),
(5, 'Ruangan Arjuna', 'Parkiran Luas, Security 24 Jam, CCTV', 'Interior Minimalis, Ruang Meeting, Video Call Room', 'Business', 1000000, 1500000, 100000, 'ceo-office-ceiling-interior-2013-modern-minimalist-ceo-office-in-modern-ceo-office-design-for-provide-house.jpg', 72, 150000, 100000, 4, '2017-09-29 23:41:46', '2017-09-29 16:41:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `saran`
--

CREATE TABLE `saran` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_users` int(11) NOT NULL,
  `isi_saran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_saran` datetime DEFAULT NULL,
  `id_ruangan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `saran`
--

INSERT INTO `saran` (`id`, `id_users`, `isi_saran`, `tgl_saran`, `id_ruangan`, `created_at`, `updated_at`) VALUES
(1, 3, 'tes', '0000-00-00 00:00:00', 1, '2017-09-30 03:01:27', '0000-00-00 00:00:00'),
(2, 3, '', '0000-00-00 00:00:00', 1, '2017-09-30 03:02:02', '2017-09-26 18:38:10'),
(5, 13, 'Coba Saran', '2017-09-30 00:00:00', 1, '2017-09-29 20:20:23', '2017-09-29 20:20:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `sections`
--

INSERT INTO `sections` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(4, 'Kebersihan Ruangan', '2017-08-14 05:30:11', '2017-08-13 22:30:11'),
(5, 'Keamanan', '2017-08-14 05:31:00', '2017-08-13 22:31:00'),
(6, 'Kebersihan Taman', '2017-08-14 05:31:39', '2017-08-13 22:31:39'),
(7, 'Keramahan Staff', '2017-09-30 18:02:35', '2017-09-30 18:02:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surveis`
--

CREATE TABLE `surveis` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal_survei` date NOT NULL,
  `waktu_survei` time NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_users` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `surveis`
--

INSERT INTO `surveis` (`id`, `tanggal_survei`, `waktu_survei`, `id_ruangan`, `created_at`, `updated_at`, `id_users`) VALUES
(2, '2017-09-06', '04:00:00', 0, '2017-09-27 00:05:40', '0000-00-00 00:00:00', 3),
(5, '2017-10-01', '09:00:00', 0, '2017-09-30 02:47:37', '2017-09-29 19:47:37', 13),
(6, '2017-01-01', '00:00:00', 1, '2017-09-30 13:51:09', '2017-09-30 06:46:01', 3),
(7, '2017-01-02', '01:01:00', 3, '2017-09-30 14:00:36', '2017-09-30 07:00:36', 13),
(8, '2018-02-02', '00:00:00', 1, '2017-09-30 06:49:07', '2017-09-30 06:49:07', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksis`
--

CREATE TABLE `transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `total_bayar` double NOT NULL,
  `tanggal_jatuh_tempo` datetime NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `jenis_bayar` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `id_kontrak` int(10) UNSIGNED DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `transaksis`
--

INSERT INTO `transaksis` (`id`, `total_bayar`, `tanggal_jatuh_tempo`, `tanggal_bayar`, `jenis_bayar`, `id_kontrak`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 20000000, '2017-09-08 00:00:00', '2017-09-22 00:00:00', 'Deposit', 2, '', '2017-09-30 22:28:43', '0000-00-00 00:00:00'),
(3, 3000000, '2017-09-08 00:00:00', '2017-09-30 00:00:00', 'Deposit', 2, '', '2017-09-30 22:28:52', '2017-09-25 08:21:12'),
(4, 4555555, '2017-09-04 00:00:00', '2017-09-19 00:00:00', 'Deposit', 2, '', '2017-09-30 22:28:58', '2017-09-26 17:37:47'),
(6, 150000, '2017-09-30 00:00:00', '2017-09-30 00:00:00', 'Biaya Overtime AC', 4, 'Pembayaran biaya overtime + biaya keterlambatan pembayaran', '2017-09-30 15:48:26', '2017-09-30 15:48:26'),
(7, 1000000, '2017-10-01 00:00:00', '2017-10-01 00:00:00', 'Deposit', 6, '', '2017-10-01 04:22:30', '2017-09-30 15:58:44'),
(8, 350000, '2017-10-01 00:00:00', '2017-10-01 00:00:00', 'Biaya Overtime AC', 6, '', '2017-09-30 21:36:49', '2017-09-30 21:36:49'),
(9, 550000, '2017-10-01 00:00:00', '2017-10-01 00:00:00', 'Pembayaran PLN', 6, '', '2017-09-30 21:37:06', '2017-09-30 21:37:06'),
(10, 200000, '2017-10-01 00:00:00', '2017-10-01 00:00:00', 'Biaya Cleaning Service', 6, '', '2017-09-30 21:38:04', '2017-09-30 21:38:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `institusi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `institusi`, `no_telp`, `jenis`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Derry Ferdians', 'derryfer@gmail.com', '$2y$10$XoO9DLFgcsE69sAMgUqGzuqh0QUOqJ3oCzDeaEh0Y/W6pbwfqg58C', 'PT Besi', '08112341234', 'penyewa', 'UReJdSU2EnLx0l1b2AUKSfw1yItfRzQhaDxa9770tDEYIhg9W7yMvlnMFdfs', '2017-08-01 09:50:22', '2017-10-01 01:14:23'),
(5, 'Yovita Rosalin', 'rosalinyovita@gmail.com', '$2y$10$ZS.awg6lTTbtZ3pEqEeiCuQDXhn2w6Q7kd.DKL7RgXDMDEOYIiBaW', 'KG Surabaya Jemur', '03112341234', 'admin', 'YMdpz5e00AkXO8pkqeZlSHXNIvdWDOuNL6oz10rNGABz2y31VhSh7oOGXBU1', '2017-08-01 17:54:46', '2017-10-01 13:33:14'),
(6, 'Yovita', 'yovitarosalin@ymail.com', '$2y$10$YSJDImgx/mXwH4cx2gbuQ.DmxL6Zl7A3AmnvWQUQy.ptIawk5UrlS', 'KG Jakarta', '02112341234', 'pimpinan', 'eYnItsHZk1U5dl0i196ovB1fVtYFjgj7JJeKdISFVveB6ouAtMzsv2Jzb1Hs', '2017-08-01 18:09:14', '2017-09-19 10:28:20'),
(10, 'Daniel', 'danski@gmail.com', '$2y$10$JyyFVyOg7AsjUB8VGSh3Desc.ft1m8keJALicPxrd2X14hoPPxMNe', 'PT Maju', '03112341234', 'penyewa', '1tePMm5snZjQCDMbpL3hvEaOomOw45pkaNU4z7xlJXTBDdA6TVsKbemh22Z8', '2017-08-02 22:02:15', '2017-09-26 17:38:57'),
(13, 'Erawan', 'abc@company.com', '$2y$10$ZS.awg6lTTbtZ3pEqEeiCuQDXhn2w6Q7kd.DKL7RgXDMDEOYIiBaW', 'IT', '08980434013', 'penyewa', '2FP5HYoY8NH4z5NCk02epedCc8OyozxUzWzgK7GT8OaU0GQNwtta7EasH30w', '2017-09-29 01:54:38', '2017-10-01 13:39:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gedungs`
--
ALTER TABLE `gedungs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gedungs_id_area_foreign` (`id_area`);

--
-- Indexes for table `isis`
--
ALTER TABLE `isis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `isis_id_kontrak_foreign` (`id_kontrak`),
  ADD KEY `isis_id_kuisioner_kepuasan_foreign` (`id_pertanyaan`);

--
-- Indexes for table `komplains`
--
ALTER TABLE `komplains`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komplains_id_users_foreign` (`id_users`),
  ADD KEY `komplains_id_kontrak_foreign` (`id_kontrak`);

--
-- Indexes for table `komplain_balas`
--
ALTER TABLE `komplain_balas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontraks`
--
ALTER TABLE `kontraks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontraks_id_users_foreign` (`id_users`),
  ADD KEY `kontraks_id_ruangan_foreign` (`id_ruangan`),
  ADD KEY `kontraks_id_perjanjian_foreign` (`id_perjanjian`);

--
-- Indexes for table `kuisioner_kepuasans`
--
ALTER TABLE `kuisioner_kepuasans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lihats`
--
ALTER TABLE `lihats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lihats_id_survei_foreign` (`id_survei`),
  ADD KEY `lihats_id_ruangan_foreign` (`id_ruangan`);

--
-- Indexes for table `manajemens`
--
ALTER TABLE `manajemens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manajemens_id_users_foreign` (`id_users`),
  ADD KEY `manajemens_id_ruangan_foreign` (`id_ruangan`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `perjanjians`
--
ALTER TABLE `perjanjians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permintaan_id_transaksi_foreign` (`id_transaksi`),
  ADD KEY `permintaan_id_ruangan_foreign` (`id_kontrak`);

--
-- Indexes for table `ruangans`
--
ALTER TABLE `ruangans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ruangans_id_gedung_foreign` (`id_gedung`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surveis`
--
ALTER TABLE `surveis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `surveis_id_users_foreign` (`id_users`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_id_kontrak_foreign` (`id_kontrak`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gedungs`
--
ALTER TABLE `gedungs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `isis`
--
ALTER TABLE `isis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `komplains`
--
ALTER TABLE `komplains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `komplain_balas`
--
ALTER TABLE `komplain_balas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kontraks`
--
ALTER TABLE `kontraks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kuisioner_kepuasans`
--
ALTER TABLE `kuisioner_kepuasans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lihats`
--
ALTER TABLE `lihats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manajemens`
--
ALTER TABLE `manajemens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perjanjians`
--
ALTER TABLE `perjanjians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ruangans`
--
ALTER TABLE `ruangans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `surveis`
--
ALTER TABLE `surveis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `gedungs`
--
ALTER TABLE `gedungs`
  ADD CONSTRAINT `gedungs_id_area_foreign` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id`);

--
-- Ketidakleluasaan untuk tabel `isis`
--
ALTER TABLE `isis`
  ADD CONSTRAINT `isis_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `isis_id_kuisioner_kepuasan_foreign` FOREIGN KEY (`id_pertanyaan`) REFERENCES `kuisioner_kepuasans` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `komplains`
--
ALTER TABLE `komplains`
  ADD CONSTRAINT `komplains_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`),
  ADD CONSTRAINT `komplains_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `kontraks`
--
ALTER TABLE `kontraks`
  ADD CONSTRAINT `kontraks_id_perjanjian_foreign` FOREIGN KEY (`id_perjanjian`) REFERENCES `perjanjians` (`id`),
  ADD CONSTRAINT `kontraks_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`),
  ADD CONSTRAINT `kontraks_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `lihats`
--
ALTER TABLE `lihats`
  ADD CONSTRAINT `lihats_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lihats_id_survei_foreign` FOREIGN KEY (`id_survei`) REFERENCES `surveis` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `manajemens`
--
ALTER TABLE `manajemens`
  ADD CONSTRAINT `manajemens_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `manajemens_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permintaan`
--
ALTER TABLE `permintaan`
  ADD CONSTRAINT `permintaan_ibfk_1` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`),
  ADD CONSTRAINT `permintaan_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksis` (`id`);

--
-- Ketidakleluasaan untuk tabel `ruangans`
--
ALTER TABLE `ruangans`
  ADD CONSTRAINT `ruangans_id_gedung_foreign` FOREIGN KEY (`id_gedung`) REFERENCES `gedungs` (`id`);

--
-- Ketidakleluasaan untuk tabel `surveis`
--
ALTER TABLE `surveis`
  ADD CONSTRAINT `surveis_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
