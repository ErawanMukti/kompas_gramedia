<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kompas Gramedia || Persewaan Kantor</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:400,700">
        <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/welcome/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/welcome/style.css') }}" rel="stylesheet">
    </head>

    <body>
        <?php
            if ( isset($errors) && count($errors) ) {                    
                echo "<div class='alert alert-danger'><strong>Terjadi Kesalahan.</strong> Silahkan lengkapi inputan anda.</div>";
            }
        ?>
        <div class="page-container">
            <h1>Persewaan Ruangan Kantor Kompas Gramedia</h1>
            {{ Form::open(array('url' => 'listruangan', 'class' => 'contact-us', 'method' => 'post')) }}
                {{ Form::select('lokasi', $area, null, array('id'=>'area')) }}
                {{ Form::select('nama_gedung', $gedung, null, array('id'=>'gedung')) }}
                <button type="submit">Cek Ruangan</button>
                <p>&nbsp;</p>
                <a href="{{ url('/login') }}">LOGIN</a> |
                <a href="{{ url('/user/add') }}">REGISTER</a>
            {{ Form::close() }}
        </div>

        <!-- Javascript -->
        <script src="{{ asset('assets/js/welcome/jquery-1.10.2.min.js') }}"></script>
        <script src="{{ asset('assets/js/welcome/jquery.backstretch.min.js') }}"></script>
    </body>
</html>

<script>
    jQuery(document).ready(function() {
        $.backstretch([
            "{{ asset('assets/img/backgrounds/1.jpg') }}",
            "{{ asset('assets/img/backgrounds/2.jpg') }}",
            "{{ asset('assets/img/backgrounds/3.jpg') }}",
        ], {duration: 3000, fade: 750});

        $('#area').on('change', function(e){
            var id = e.target.value;
            //alert(id)
            $.get('{{ url('jsarea')}}/'+id, function(data){
                console.log(id);
               // console.log(data);
                $('#gedung').empty();
                $.each(data, function(index, element){
                    $('#gedung').append("<option value='"+element.id+"'>"+element.nama_gedung+"</option");
                });
            });
        });
    });    
</script>
