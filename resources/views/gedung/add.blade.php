@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Gedung Perkantoran</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        {{ Form::open(array('url' => 'gedung', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('id_area') ? ' has-error' : '' }}">
                <label for="id_area" class="col-md-4 control-label">Pilih Area :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_area">
                        @foreach($area as $item)
                            <option value="{{$item->id}}">{{$item->lokasi}}</option>
                            @endforeach
                    </select>

                    @if ($errors->has('id_area'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_area') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nama_gedung') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Nama Gedung :</label>

                <div class="col-md-6">
                    <input id="nama_gedung" type="text" class="form-control" name="nama_gedung" value="{{ old('nama_gedung') }}">

                    @if ($errors->has('nama_surveyor'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_surveyor') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

             <div class="form-group{{ $errors->has('alamat_gedung') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Alamat Gedung :</label>

                <div class="col-md-6">
                 <textarea class="form-control" name="alamat_gedung">{{old('alamat_gedung')}}</textarea>

                    @if ($errors->has('alamat_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('no_telp_gedung') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Nomor Telepon:</label>

                <div class="col-md-6">
                    <input id="no_telp_gedung" type="text" class="form-control" name="no_telp_gedung" value="{{ old('no_telp_gedung') }}">

                    @if ($errors->has('no_telp_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_telp_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>

        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>
@endsection
