@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Area</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($gedung, array('route' => array('gedung.update', $gedung->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('nama_gedung', 'Nama Gedung', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                <div class="col-md-6">
                    {{ Form::text('nama_gedung', null, array('class' => 'form-control') )}}

                    @if ($errors->has('nama_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('alamat_gedung', 'Alamat Gedung', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                <div class="col-md-6">
                    {{ Form::text('alamat_gedung', null, array('class' => 'form-control') )}}

                    @if ($errors->has('alamat_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('no_telp_gedung', 'Nomor Telepon Gedung', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                <div class="col-md-6">
                    {{ Form::text('no_telp_gedung', null, array('class' => 'form-control') )}}

                    @if ($errors->has('no_telp_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_telp_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {!! form::close() !!}
    </div>
</div>
@endsection
