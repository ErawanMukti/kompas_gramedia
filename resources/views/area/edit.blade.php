@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Area</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($area, array('route' => array('area.update', $area->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('lokasi', 'Lokasi Area Gedung', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('lokasi', null, array('class' => 'form-control') )}}

                    @if ($errors->has('lokasi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lokasi') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>
@endsection
