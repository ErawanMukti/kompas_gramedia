@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Area</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'area', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('lokasi') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Lokasi Area Gedung :</label>

                <div class="col-md-6">
                    <input id="lokasi" type="text" class="form-control" name="lokasi" value="{{ old('lokasi') }}">

                    @if ($errors->has('lokasi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lokasi') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>
@endsection
