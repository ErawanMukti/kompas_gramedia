@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Saran Surveyor Ruangan Kompas Gramedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>Ruangan</td>
                    <td>Isi Saran</td>
                    <td width="100">Tanggal</td>
                    <td width="60">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($saran as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->isi_saran }}</td>
                    <td>{{ date('d/m/Y', strtotime($item->tgl_saran)) }}</td>
                    <td>
                        {{ Form::open(array('url' => 'saran/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if ( Auth::user()->jenis != 'admin' )
            <a href="{{ url('/saran/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
