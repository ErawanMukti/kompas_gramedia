@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Saran</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'saran', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('id_ruangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Ruangan :</label>

                <div class="col-md-6">
                    {{ Form::select('id_ruangan', $ruangan, null, array('class' => 'form-control col-md-7 col-xs-12')) }}

                    @if ($errors->has('id_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group{{ $errors->has('isi_saran') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Isi Saran :</label>

                <div class="col-md-6">
                    <input id="isi_saran" type="text" class="form-control" name="isi_saran" value="{{ old('isi_saran') }}">

                    @if ($errors->has('isi_saran'))
                        <span class="help-block">
                            <strong>{{ $errors->first('isi_saran') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group{{ $errors->has('tgl_saran') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Saran :</label>

                <div class="col-md-6">
                    <input id="tgl_saran" type="date" class="form-control" name="tgl_saran" value="{{ date('Y-m-d') }}">

                    @if ($errors->has('tgl_saran'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tgl_saran') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
