@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($ruangan, array('route' => array('ruangan.update', $ruangan->id), 'method' => 'PUT', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {{ Form::label('nama', 'Nama Ruangan', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('nama', null, array('class' => 'form-control') )}}

                    @if ($errors->has('nama'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fasilitas_umum') ? ' has-error' : '' }}">
                {{ Form::label('fasilitas_umum', 'Fasilitas Umum', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('fasilitas_umum', null, array('class' => 'form-control') )}}

                    @if ($errors->has('fasilitas_umum'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fasilitas_umum') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fasilitas_dalam_ruangan') ? ' has-error' : '' }}">
                {{ Form::label('fasilitas_dalam_ruangan', 'Fasilitas Dalam Ruangan', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('fasilitas_dalam_ruangan', null, array('class' => 'form-control') )}}

                    @if ($errors->has('fasilitas_dalam_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fasilitas_dalam_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('jenis_ruangan') ? ' has-error' : '' }}">
                {{ Form::label('jenis_ruangan', 'Jenis Ruangan', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('jenis_ruangan', null, array('class' => 'form-control') )}}

                    @if ($errors->has('jenis_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenis_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('deposit') ? ' has-error' : '' }}">
                {{ Form::label('deposit', 'Deposit', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('deposit', null, array('class' => 'form-control') )}}

                    @if ($errors->has('deposit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('deposit') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('harga_perm') ? ' has-error' : '' }}">
                {{ Form::label('harga_perm', 'Harga Per Meter', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('harga_perm', null, array('class' => 'form-control') )}}

                    @if ($errors->has('harga_perm'))
                        <span class="help-block">
                            <strong>{{ $errors->first('harga_perm') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('service_charge') ? ' has-error' : '' }}">
                {{ Form::label('service_charge', 'Service Charge', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('service_charge', null, array('class' => 'form-control') )}}

                    @if ($errors->has('service_charge'))
                        <span class="help-block">
                            <strong>{{ $errors->first('service_charge') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                {{ Form::label('foto', 'Foto', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::file('foto', null, array('class' => 'form-control') )}}

                    @if ($errors->has('foto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('foto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('luas_ruangan') ? ' has-error' : '' }}">
                {{ Form::label('luas_ruangan', 'Luas Ruangan', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('luas_ruangan', null, array('class' => 'form-control') )}}

                    @if ($errors->has('luas_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('luas_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('biaya_overtime_ac') ? ' has-error' : '' }}">
                {{ Form::label('biaya_overtime_ac', 'Biaya Overtime AC', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('biaya_overtime_ac', null, array('class' => 'form-control') )}}

                    @if ($errors->has('biaya_overtime_ac'))
                        <span class="help-block">
                            <strong>{{ $errors->first('biaya_overtime_ac') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pajak_pph') ? ' has-error' : '' }}">
                {{ Form::label('pajak_pph', 'Pajak PPH', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('pajak_pph', null, array('class' => 'form-control') )}}

                    @if ($errors->has('pajak_pph'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pajak_pph') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {!! form::close() !!}
    </div>
</div>
@endsection
