@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Permintaan Ruangan Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Line Telepon</td>
                                <td>Daya Listrik</td>
                                <td>Cleaning Service</td>
                                <td>Reklame</td>
                                <td>Kegiatan</td>
                                <td>Renovasi</td>
                                <td>Sewa</td>
                                <td>Lain Lain</td>
                                <td>ruangan</td>
                                <td>transaksi</td>
                                <td>Action</td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ruangan as $item)
                            <tr>
                                <td>{{ $item->id }}</td>                                
                                <td>{{ $item->linetelepon }}</td>
                                <td>{{ $item->dayalistrik }}</td>
                                <td>{{ $item->cleaning }}</td>
                                <td>{{ $item->reklame }}</td>
                                <td>{{ $item->kegiatan }}</td>
                                <td>{{ $item->renovasi }}</td>
                                <td>{{ $item->sewa }}</td>
                                <td>{{ $item->lain }}</td>
                                <td>{{ $item->id_ruangan }}</td>
                                <td>{{ $item->id_transaksi }}</td>
                                <td>
                                    <form method="POST" action="{{ url('permintaan/destroy', $item->id) }}" accept-charset="UTF-8">
                                     <a class="btn btn-xs btn-info" href="{{ url('permintaan/edit', $item->id) }}">Ubah</a>
                                     <a class="btn btn-xs btn-info" href="{{ url('permintaan/edit', $item->id) }}">Tambah</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='admin')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Line Telepon</td>
                                <td>Daya Listrik</td>
                                <td>Cleaning Service</td>
                                <td>Reklame</td>
                                <td>Kegiatan</td>
                                <td>Renovasi</td>
                                <td>Sewa</td>
                                <td>Lain Lain</td>
                                <td>ruangan</td>
                                <td>transaksi</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ruangan as $item)
                            <tr>
                                <td>{{ $item->id }}</td>                                
                                <td>{{ $item->linetelepon }}</td>
                                <td>{{ $item->dayalistrik }}</td>
                                <td>{{ $item->cleaning }}</td>
                                <td>{{ $item->reklame }}</td>
                                <td>{{ $item->kegiatan }}</td>
                                <td>{{ $item->renovasi }}</td>
                                <td>{{ $item->sewa }}</td>
                                <td>{{ $item->lain }}</td>
                                <td>{{ $item->id_ruangan }}</td>
                                <td>{{ $item->id_transaksi }}</td>
                                <td>
                                    <form method="POST" action="{{ url('permintaan/destroy', $item->id) }}" accept-charset="UTF-8">                                   
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
