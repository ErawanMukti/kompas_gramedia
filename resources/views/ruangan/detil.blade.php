@extends('layouts.lte')

@section('content')
<div class="container"><div class="box">
    <div class="box-header">
      <h3 class="box-title">Informasi Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('ruangan/update', ['id' => $ruangan->id]) }}">
            <div class="form-group">
                {!! Form::label('lokasi', 'Area Lokasi', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   {{ $ruangan->lokasi }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('gedung', 'Nama Gedung', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   {{ $ruangan->nama_gedung }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('nama', 'Nama Ruangan', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   {{ $ruangan->nama }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('fasilitas_umum', 'Fasilitas Umum', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ $ruangan->fasilitas_umum }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('fasilitas_dalam_ruangan', 'Fasilitas Dalam Ruangan', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ $ruangan->fasilitas_dalam_ruangan }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('luas_ruangan', 'Luas Ruangan', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ $ruangan->luas_ruangan }} m2
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('pajak_pph', 'Pajak PPH', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    Rp. {{ number_format($ruangan->pajak_pph) }}</span>,-
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('biaya_overtime_ac', 'Biaya Overtime AC', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    Rp. {{ number_format($ruangan->biaya_overtime_ac) }}</span>,-
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('harga_perm', 'Harga Per Meter', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    Rp. {{ number_format($ruangan->harga_perm) }}</span>,-
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('id_gedung', 'Status', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {!! (count($_ruangan->kontrak) >= 1) ? '<label class="btn-xs btn-danger">Sedang Ditempati</label>' : '<label class="btn-xs btn-success">Tersedia</label>' !!}
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

