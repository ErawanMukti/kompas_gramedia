@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ruang Kantor Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nama</td>
                                <td>Jenis Ruangan</td>
                                <td>Harga Per Meter</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ruangan as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_ruangan }}</td>
                                <td>{{ $item->harga_perm }}</td>
                                <td>
                                    <form method="POST" action="{{ url('ruangan/destroy', $item->id) }}" accept-charset="UTF-8">
                                    <a class="btn btn-xs btn-info" href="{{ url('ruangan/detil', $item->id) }}">Detail</a>
                                    <!--<a class="btn btn-xs btn-info" href="{{ url('ruangan/edit', $item->id) }}">Ubah</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />-->
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='pimpinan')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nama</td>
                                <td>Jenis Ruangan</td>
                                <td>Harga Per Meter</td>
                                <td>Status</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ruangan as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_ruangan }}</td>
                                <td>{{ $item->harga_perm }}</td>
                                 <td>
                                    <a class="btn btn-xs btn-info" href="{{ url('ruangan/detil', $item->id) }}">Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <!--<a href="{{ url('/ruangan/add') }}" class="btn btn-info">Tambah</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
