@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ruang Kantor Kompas Gramedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>Nama</td>
                    <td>Jenis Ruangan</td>
                    <td>Harga Per Meter</td>
                    <td width="150">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($ruangan as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->jenis_ruangan }}</td>
                    <td>{{ number_format($item->harga_perm) }}</td>
                    <td>
                        {{ Form::open(array('url' => 'ruangan/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('ruangan/'. $item->id .'/detail') }}" title="Detail Ruangan"><button type='button' class='btn btn-default'><i class='fa fa-search'></i></button></a>
                            @if( Auth::user()->jenis == 'admin' )
                            <a href="{{ url('ruangan/'. $item->id .'/edit') }}" title="Ubah Data"><button type='button' class='btn btn-default'><i class='fa fa-edit'></i></button></a>
                            {{ (count($item->kontrak) == 0) ? Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) : '' }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->jenis=='admin')
            <a href="{{ url('/ruangan/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
</div>
@endsection
