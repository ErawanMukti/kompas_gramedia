@extends('layouts.lte')

@section('content')
<div class="container"><div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'ruangan', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('id_gedung') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Pilih Gedung :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_gedung">
                        @foreach($gedung as $item)
                            <option value="{{$item->id}}">{{$item->nama_gedung}}</option>
                            @endforeach
                    </select>

                    @if ($errors->has('id_gedung'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_gedung') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Nama Ruangan :</label>

                <div class="col-md-6">
                    <input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}">

                    @if ($errors->has('nama'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fasilitas_umum') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Fasilitas Umum Ruangan :</label>

                <div class="col-md-6">
                    <input id="fasilitas_umum" type="text" class="form-control" name="fasilitas_umum" value="{{ old('fasilitas_umum') }}">

                    @if ($errors->has('fasilitas_umum'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fasilitas_umum') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fasilitas_dalam_ruangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Fasilitas Dalam Ruangan :</label>

                <div class="col-md-6">
                    <input id="fasilitas_dalam_ruangan" type="text" class="form-control" name="fasilitas_dalam_ruangan" value="{{ old('fasilitas_dalam_ruangan') }}">

                    @if ($errors->has('fasilitas_dalam_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fasilitas_dalam_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('jenis_ruangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Jenis Ruangan :</label>

                <div class="col-md-6">
                    <input id="jenis_ruangan" type="text" class="form-control" name="jenis_ruangan" value="{{ old('jenis_ruangan') }}">

                    @if ($errors->has('jenis_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenis_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('deposit') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Deposit :</label>

                <div class="col-md-6">
                    <input id="deposit" type="text" class="form-control" name="deposit" value="{{ old('deposit') }}">

                    @if ($errors->has('deposit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('deposit') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('harga_perm') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Harga Per Meter :</label>

                <div class="col-md-6">
                    <input id="harga_perm" type="text" class="form-control" name="harga_perm" value="{{ old('harga_perm') }}">

                    @if ($errors->has('harga_perm'))
                        <span class="help-block">
                            <strong>{{ $errors->first('harga_perm') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('service_charge') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Service Charge :</label>

                <div class="col-md-6">
                    <input id="service_charge" type="text" class="form-control" name="service_charge" value="{{ old('service_charge') }}">

                    @if ($errors->has('service_charge'))
                        <span class="help-block">
                            <strong>{{ $errors->first('service_charge') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Foto :</label>

                <div class="col-md-6">
                    <input id="foto" type="file" class="form-control" name="foto" value="{{ old('foto') }}">

                    @if ($errors->has('foto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('foto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('luas_ruangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Luas Ruangan per Meter:</label>

                <div class="col-md-6">
                    <input id="luas_ruangan" type="text" class="form-control" name="luas_ruangan" value="{{ old('luas_ruangan') }}">

                    @if ($errors->has('luas_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('luas_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('biaya_overtime_ac') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Biaya Overtime AC :</label>

                <div class="col-md-6">
                    <input id="biaya_overtime_ac" type="text" class="form-control" name="biaya_overtime_ac" value="{{ old('biaya_overtime_ac') }}">

                    @if ($errors->has('biaya_overtime_ac'))
                        <span class="help-block">
                            <strong>{{ $errors->first('biaya_overtime_ac') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pajak_pph') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Pajak PPH :</label>

                <div class="col-md-6">
                    <input id="pajak_pph" type="text" class="form-control" name="pajak_pph" value="{{ old('pajak_pph') }}">

                    @if ($errors->has('pajak_pph'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pajak_pph') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
