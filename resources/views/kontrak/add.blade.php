@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Kontrak Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'kontrak', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
            {{ csrf_field() }}
            
            <div class="form-group{{ $errors->has('no_kontrak') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label"> No Kontrak :</label>

                <div class="col-md-6">
                    <input id="no_kontrak" type="text" class="form-control" name="no_kontrak" value="{{ old('no_kontrak') }}">

                    @if ($errors->has('no_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_users') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">User yang Teribat :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_users">
                        <option value=""></option>
                        @foreach($user as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_users'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_users') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_ruangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Ruangan yang Disewa :</label>

                 <div class="col-md-6">
                    <select class="form-control" name="id_ruangan">
                        <option value=""></option>
                        @foreach($ruangan as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_perjanjian') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Perjanjian :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_perjanjian">
                        <option value=""></option>
                        @foreach($perjanjian as $item)
                            <option value="{{$item->id}}">{{$item->nama_perjanjian}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('isi_pasal') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label"> File Kontrak :</label>

                <div class="col-md-6">
                    <input id="file_kontrak" type="file" class="form-control" name="file_kontrak" value="{{ old('file_kontrak') }}">

                    @if ($errors->has('file_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('file_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('jangka_waktu') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Jangka Waktu :</label>

                <div class="col-md-6">
                    <input id="jangka_waktu" type="number" class="form-control" name="jangka_waktu" value="{{ old('jangka_waktu') }}" onchange="hitung_waktu()">

                    @if ($errors->has('jangka_waktu'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jangka_waktu') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_masuk') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Masuk :</label>

                <div class="col-md-6">
                    <input id="tanggal_masuk" type="date" class="form-control" name="tanggal_masuk" value="{{ old('tanggal_masuk') }}" onchange="hitung_waktu()">

                    @if ($errors->has('tanggal_masuk'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_masuk') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_keluar') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Keluar :</label>

                <div class="col-md-6">
                    <input id="tanggal_keluar" type="date" class="form-control" name="tanggal_keluar" value="{{ old('tanggal_keluar') }}" readonly>

                    @if ($errors->has('tanggal_keluar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_keluar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('js_content')
<script>
    function hitung_waktu($nilai){
        var jangka = document.getElementById('jangka_waktu').value;
        var jangka_tahun = jangka * 365;

        var chkin  = document.getElementById('tanggal_masuk').value;
        var chkin_date = new Date(chkin);
        var chkot_date = new Date(chkin_date.getTime() + jangka_tahun * 24 * 60 * 60 * 1000);

        var thn = chkot_date.getFullYear();
        var bln = chkot_date.getMonth() + 1;
        var tgl = chkot_date.getDate();

        document.getElementById('tanggal_keluar').value = thn + "-" + addZero(bln) + "-" + addZero(tgl);
    }

    function addZero(i) {
        if ( i<10 ) {
            i = "0" + i;
        }
        return i;
    }
</script>
@endsection