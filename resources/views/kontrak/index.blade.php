@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Kontrak Ruangan Kompas Gramedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>No Kontrak</td>
                    <td>No Perjanjian</td>
                    <td>Jangka Waktu (tahun)</td>
                    <td>Tanggal Masuk</td>
                    <td>Tanggal Keluar</td>
                    <td width="150">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($kontrak as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('/fileKontrak/').'/'.$item->file_kontrak }}"> {{ $item->no_kontrak }} </td>
                    <td><a href="{{ url('/isiPasal/').'/'.$item->perjanjian->isi_pasal }}">{{ $item->perjanjian->no_perjanjian }}</a></td>
                    <td>{{ $item->jangka_waktu }}</td>
                    <td>{{ $item->tanggal_masuk }}</td>
                    <td>{{ $item->tanggal_keluar }}</td>
                    <td>
                        {{ Form::open(array('url' => 'kontrak/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('kontrak/'. $item->id .'/detail') }}" title="Detail Kontrak"><button type='button' class='btn btn-default'><i class='fa fa-search'></i></button></a>
                            @if(Auth::user()->jenis=='admin')
                                <a href="{{ url('kontrak/'. $item->id .'/edit') }}" title="Ubah Data"><button type='button' class='btn btn-default'><i class='fa fa-edit'></i></button></a>
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->jenis=='admin')
            <a href="{{ url('/kontrak/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
