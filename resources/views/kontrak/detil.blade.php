@extends('layouts.lte')

@section('content')
<div class="container"><div class="box">
    <div class="box-header">
      <h3 class="box-title">Detail Kontrak Kontrak Kompas Gramedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                {!! Form::label('no_kontrak', 'No Kontrak', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   {{ $kontrak->no_kontrak }}</a>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('nama_perjanjian', 'Nama Perjanjian', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   <a href="{{ url('/isiPasal/').'/'.$kontrak->isi_pasal }}">{{ $kontrak->nama_perjanjian }}</a>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('nama', 'Nama Ruangan', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   {{ $kontrak->nama }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('file_kontrak', 'File Kontrak', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                   <a href="{{ url('/fileKontrak/').'/'.$kontrak->file_kontrak }}">{{ $kontrak->file_kontrak }}</a>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('jangka_waktu', 'Jangka Waktu', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ $kontrak->jangka_waktu }} Tahun
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('tanggal_masuk', 'Tanggal Masuk', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ date('d/m/Y', strtotime($kontrak->tanggal_masuk)) }}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('tanggal_keluar', 'Tanggal Keluar', ['class'=>'col-md-2 col-md-offset-1']) !!}

                <div class="col-md-6">
                    {{ date('d/m/Y', strtotime($kontrak->tanggal_keluar)) }}
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

