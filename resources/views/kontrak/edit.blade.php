@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Kontrak Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($kontrak, array('route' => array('kontrak.update', $kontrak->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}
                        
            <div class="form-group{{ $errors->has('no_kontrak') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">No Kontrak :</label>

                <div class="col-md-6">
                    <input id="no_kontrak" type="text" class="form-control" name="no_kontrak" value="{{ $kontrak->no_kontrak }}">

                    @if ($errors->has('no_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_perjanjian') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Perjanjian :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_perjanjian">
                        @foreach($perjanjian as $item)
                            @if ( $item->id == $kontrak->id_perjanjian )
                                <option value="{{$item->id}}" selected>{{$item->nama_perjanjian}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->nama_perjanjian}}</option>
                            @endif
                        @endforeach
                    </select>

                    @if ($errors->has('id_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('isi_pasal') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label"> File Kontrak :</label>

                <div class="col-md-6">
                    <input id="file_kontrak" type="file" class="form-control" name="file_kontrak" value="{{ old('file_kontrak') }}">

                    @if ($errors->has('file_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('file_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('jangka_waktu') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Jangka Waktu :</label>

                <div class="col-md-6">
                    <input id="jangka_waktu" type="text" class="form-control" name="jangka_waktu" value="{{ $kontrak->jangka_waktu }}" onchange="hitung_waktu()">

                    @if ($errors->has('jangka_waktu'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jangka_waktu') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_masuk') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Masuk :</label>

                <div class="col-md-6">
                    <input id="tanggal_masuk" type="date" class="form-control" name="tanggal_masuk" value="{{ date('Y-m-d', strtotime($kontrak->tanggal_masuk)) }}" onchange="hitung_waktu()">

                    @if ($errors->has('tanggal_masuk'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_masuk') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_keluar') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Keluar :</label>

                <div class="col-md-6">
                    <input id="tanggal_keluar" type="date" class="form-control" name="tanggal_keluar" value="{{ date('Y-m-d', strtotime($kontrak->tanggal_keluar)) }}" readonly>

                    @if ($errors->has('tanggal_keluar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_keluar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('js_content')
<script>
    function hitung_waktu($nilai){
        var jangka = document.getElementById('jangka_waktu').value;
        var jangka_tahun = jangka * 365;

        var chkin  = document.getElementById('tanggal_masuk').value;
        var chkin_date = new Date(chkin);
        var chkot_date = new Date(chkin_date.getTime() + jangka_tahun * 24 * 60 * 60 * 1000);

        var thn = chkot_date.getFullYear();
        var bln = chkot_date.getMonth() + 1;
        var tgl = chkot_date.getDate();

        document.getElementById('tanggal_keluar').value = thn + "-" + addZero(bln) + "-" + addZero(tgl);
    }

    function addZero(i) {
        if ( i<10 ) {
            i = "0" + i;
        }
        return i;
    }
</script>
@endsection