@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/') }}">
                  
                        <div class="form-group">
                            <label for="text" class="col-md-4 control-label">Silahkan Menghubungi Admin Untuk Mereset Password Anda !</label>

                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Kembali Ke Home
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
