@extends('layouts.lte')

@section('content')
<div class="container"><div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Permintaan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'permintaan', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}
                 
            <div class="form-group{{ $errors->has('id_users') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">User yang Teribat :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_users">
                        <option value="{{$user->id}}">{{$user->nama}}</option>
                    </select>

                    @if ($errors->has('id_users'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_users') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_kontrak') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">No Kontrak :</label>

                 <div class="col-md-6">
                    <select class="form-control" name="id_kontrak">
                        @foreach($kontrak as $item)
                            <option value="{{$item->id}}">{{$item->no_kontrak}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nama_permintaan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Nama Permintaan :</label>

                <div class="col-md-6">
                    {{
                        Form::select('nama_permintaan', array(
                            'Tambah Line Telepon' => 'Tambah Line Telepon',
                            'Tambah Daya Listrik' => 'Tambah Daya Listrik',
                            'Cleaning Service'    => 'Cleaning Service',
                            'Bikin Reklame'       => 'Bikin Reklame',
                            'Event'               => 'Event',
                            'Renovasi'            => 'Renovasi',
                            'Perpanjangan Sewa'   => 'Perpanjangan Sewa',
                            'Sewa Ruangan Baru'   => 'Sewa Ruangan Baru',
                            'Lain-Lain'           => 'Lain-Lain'
                        ), Input::old('nama_permintaan'), array('class' => 'form-control'))
                    }}

                    @if ($errors->has('nama_permintaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_permintaan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group{{ $errors->has('isi_permintaan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Isi Permintaan :</label>

                <div class="col-md-6">
                    <input id="isi_permintaan" type="text" class="form-control" name="isi_permintaan" value="{{ old('isi_permintaan') }}">

                    @if ($errors->has('isi_permintaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('isi_permintaan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_pengajuan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Pengajuan :</label>

                <div class="col-md-6">
                    <input id="tanggal_pengajuan" type="date" class="form-control" name="tanggal_pengajuan" value="{{ date('Y-m-d') }}">

                    @if ($errors->has('tanggal_pengajuan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_pengajuan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>                         
                        
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
