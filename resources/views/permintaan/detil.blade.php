@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ruang Kantor Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                     @foreach($kontrak as $item)
                    <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <td>Nama</td>
                                <td>{{ $item->nama }}</td>
                            </tr>

                            <tr>
                                <td>Jangka Waktu Kontrak</td>
                                <td>{{ $item->jangka_waktu }} tahun</td>
                            </tr>

                            <tr>
                                <td>Tanggal Masuk</td>
                                <td>{{ $item->tanggal_masuk }}</td>
                            </tr>

                            <tr>
                                <td>Tanggal Keluar</td>
                                <td>{{ $item->tanggal_keluar }}</td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
