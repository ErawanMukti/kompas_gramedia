@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Permintaan Ruangan Kompas Gramedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        @if( $block == true )
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Perhatian!</h4>
                Silahkan melakukan pembayaran bulan ini untuk dapat menggunakan fasilitas ini
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Permintaan</td>
                    <td>Tanggal Pengajuan</td>
                    <td>No Kontrak</td>
                    <td>Status</td>
                    <td width="150">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($permintaan as $item)
                <tr>
                    <td>{{ $item->id }}</td>   
                    <td>{{ $item->nama_permintaan}} </td>
                    <td>{{ date('d/m/Y', strtotime($item->tanggal_pengajuan)) }}</td>
                    <td>{{ $item->no_kontrak }}</td>
                    @if ( $item->status == 0 )
                        <td>Pending</td>
                    @elseif ( $item->status == 1 )
                        <td>Accept</td>
                    @else
                        <td>Reject</td>
                    @endif
                    <td>
                        {{ Form::open(array('url' => 'permintaan/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('permintaan/'. $item->id .'/nota') }}" title="Cetak" target="_blank"><button type='button' class='btn btn-default'><i class='fa fa-print'></i></button></a>
                            @if(Auth::user()->jenis == 'admin')
                                <a href="{{ url('permintaan/'. $item->id .'/accept') }}" title="Setujui Permintaan"><button type='button' class='btn btn-default'><i class='fa fa-check'></i></button></a>
                                <a href="{{ url('permintaan/'. $item->id .'/denied') }}" title="Tolak Permintaan"><button type='button' class='btn btn-default'><i class='fa fa-minus-square'></i></button></a>
                            @else
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if( Auth::user()->jenis == 'penyewa' && $block == false)
            <a href="{{ url('/permintaan/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
