@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Kontrak Ruangan Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>File Kontrak</td>
                                <td>Jangka Waktu (tahun)</td>
                                <td>Tanggal Masuk</td>
                                <td>Tanggal Keluar</td>
                                <!--<td>Action</td>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kontrak as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><a href="{{ url('/fileKontrak/').'/'.$item->file_kontrak }}"> {{ $item->file_kontrak}} </td>
                                <td>{{ $item->jangka_waktu }}</td>
                                <td>{{ $item->tanggal_masuk }}</td>
                                <td>{{ $item->tanggal_keluar }}</td>
                                <!--<td>
                                    <form method="POST" action="{{ url('kontrak/destroy', $item->id) }}" accept-charset="UTF-8">
                                     <a class="btn btn-xs btn-info" href="{{ url('kontrak/edit', $item->id) }}">Ubah</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Jangka Waktu (tahun)</td>
                                <td>Tanggal Masuk</td>
                                <td>Tanggal Keluar</td>
                                <td>ID User yang Terlibat</td>
                                <td>ID Ruangan yang disewa</td>
                                <td>ID Perjanjian</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kontrak as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->jangka_waktu }}</td>
                                <td>{{ $item->tanggal_masuk }}</td>
                                <td>{{ $item->tanggal_keluar }}</td>
                                <td>{{ $item->id_users }}</td>
                                <td>{{ $item->id_ruangan }}</td>
                                <td>{{ $item->id_perjanjian }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <!--<a href="{{ url('/kontrak/add') }}" class="btn btn-info">Tambah</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
