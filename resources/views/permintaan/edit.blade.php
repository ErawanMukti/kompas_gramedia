@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Permintaan Ruangan</div>

                    <div class="panel-body">
                    {!! Form::model($permintaan, ['url'=>['permintaan/update', $permintaan->id], 'enctype'=>'multipart/form-data', 'method'=>'POST']) !!}
                        
                        <div class="form-group{{ $errors->has('id_users') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">User yang Teribat :</label>

                            <div class="col-md-6">
                                <select class="form-control" name="id_users">
                                    @foreach($user as $item)
                                        <option value="{{$item->id}}">{{$item->nama}}</option>
                                        @endforeach
                                </select>

                                @if ($errors->has('id_users'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_users') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('id_ruangan') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Ruangan yang Disewa :</label>

                             <div class="col-md-6">
                                <select class="form-control" name="id_ruangan">
                                    @foreach($ruangan as $item)
                                        <option value="{{$item->id}}">{{$item->nama}}</option>
                                        @endforeach
                                </select>

                                @if ($errors->has('id_ruangan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_ruangan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('id_perjanjian') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Perjanjian :</label>

                            <div class="col-md-6">
                                <select class="form-control" name="id_perjanjian">
                                    @foreach($perjanjian as $item)
                                        <option value="{{$item->id}}">{{$item->nama_perjanjian}}</option>
                                        @endforeach
                                </select>

                                @if ($errors->has('id_perjanjian'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_perjanjian') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                         <div class="form-group{{ $errors->has('isi_pasal') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label"> File Permintaan :</label>

                            <div class="col-md-6">
                                <input id="isi_permintaan" type="file" class="form-control" name="isi_permintaan" value="{{ old('isi_permintaan') }}">

                                @if ($errors->has('isi_permintaan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('isi_permintaan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tanggal_pengajuan') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Tanggal Pengajuan :</label>

                            <div class="col-md-6">
                                <input id="tanggal_pengajuan" type="date" class="form-control" name="tanggal_pengajuan" value="{{ old('tanggal_pengajuan') }}">

                                @if ($errors->has('tanggal_pengajuan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tanggal_pengajuan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::submit('Ubah', ['class'=>'btn btn-info']) !!}
                        </div>
                    {!! form::close() !!}
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
