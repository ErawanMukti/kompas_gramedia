@extends('layouts.cetak')

@section('content')
    <h3>Bukti Permintaan Persewaan Gedung Kompas Gramedia</h3>
    <table class="table table-bordered table-striped">
        <tr>
            <td width="200">No Permintaan</td>
            <td>{{ $permintaan->id }}</td>   
        </tr>
        <tr>
            <td>Tanggal Permintaan</td>
            <td>{{ date('d/m/Y', strtotime($permintaan->tanggal_pengajuan)) }}</td>   
        </tr>
        <tr>
            <td>Permintaan</td>
            <td>{{ $permintaan->nama_permintaan }}</td>
        </tr>
        <tr>
            <td>Isi Permintaan</td>
            <td>{{ $permintaan->isi_permintaan }}</td>
        </tr>
        <tr>
            <td>No Kontrak</td>
            <td>{{ $kontrak->no_kontrak }}</td>
        </tr>
        <tr>
            <td>Nama Ruangan</td>
            <td>{{ $ruangan->nama }}</td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="75%"></td>
            <td>
                <center>
                    <p>Surabaya, {{ date('d M Y') }}</p>
                    <p><img src="{{ asset('assets/img/logo.png') }}" style="width: 100%; height: auto;"></p>
                    <p>Manajemen Pergedungan Kompas Media</p>
                </center>
            </td>
        </tr>
    </table>

@endsection