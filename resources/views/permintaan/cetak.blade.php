@extends('layouts.cetak')

@section('content')
    <h3>Laporan Permintaan User</h3>
    <p>
        Jenis Permintaan: {{ $jenis }}, 
        Periode Laporan: {{ date('d/m/Y', strtotime($tgl)) }}
    </p>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>User</td>
                <td>No Kontrak</td>
                <td>Nama Ruangan</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
            @foreach($permintaan as $item)
            <tr>
                <td>{{ $item->id }}</td>   
                <td>{{ $item->email}} </td>
                <td>{{ $item->no_kontrak }}</td>
                <td>{{ $item->nama }}</td>
                @if ( $item->status == 0 )
                    <td>Pending</td>
                @elseif ( $item->status == 1 )
                    <td>Accept</td>
                @else
                    <td>Reject</td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
