@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Perjanjian Sewa - Menyewa Ruangan</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nama Perjanjian</td>
                                <!--<td>Action</td>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($perjanjian as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><a href="{{ url('/isiPasal/').'/'.$item->isi_pasal }}"> {{ $item->nama_perjanjian}} </td>
                                <!--<td>
                                    <form method="POST" action="{{ url('perjanjian/destroy', $item->id) }}" accept-charset="UTF-8">
                                    <a class="btn btn-xs btn-info" href="{{ url('perjanjian/edit', $item->id) }}">Ubah</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='pimpinan')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nama Perjanjian</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($perjanjian as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><a href="{{ url('/isiPasal/').'/'.$item->isi_pasal }}"> {{ $item->nama_perjanjian}} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                   @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <!--<a href="{{ url('/perjanjian/add') }}" class="btn btn-info">Tambah</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
