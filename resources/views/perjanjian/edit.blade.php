@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Perjanjian</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($perjanjian, array('route' => array('perjanjian.update', $perjanjian->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('no_perjanjian', 'No Perjanjian', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('no_perjanjian', null, array('class' => 'form-control') )}}

                    @if ($errors->has('no_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('nama_perjanjian', 'Nama Perjanjian', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('nama_perjanjian', null, array('class' => 'form-control') )}}

                    @if ($errors->has('nama_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('isi_pasal', 'Isi Pasal', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::file('isi_pasal', null, array('class' => 'form-control') )}}

                    @if ($errors->has('isi_pasal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('isi_pasal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
