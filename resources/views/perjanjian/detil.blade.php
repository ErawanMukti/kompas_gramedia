@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Detil Perjanjian Ruangan</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    <table id="example1" class="table table-bordered table-striped">
                    @foreach($ruangan as $item)
                            <tr>
                                <td>Nama</td>
                                <td>{{ $item->nama }}</td>
                            </tr>
                          
                            <tr>
                                <td>Isi Pasal</td>
                                <td>{{ $item->isi_pasal }}</td>
                            </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
