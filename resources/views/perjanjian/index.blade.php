@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Perjanjian Sewa - Menyewa Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>No Perjanjian</td>
                    <td>Nama Perjanjian</td>
                    @if(Auth::user()->jenis=='admin')
                        <td width="100">Action</td>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($perjanjian as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->no_perjanjian }}</td>
                    <td><a href="{{ url('/isiPasal/').'/'.$item->isi_pasal }}"> {{ $item->nama_perjanjian}} </td>
                    @if(Auth::user()->jenis=='admin')
                        <td>
                            {{ Form::open(array('url' => 'perjanjian/'.$item->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <a href="{{ url('perjanjian/'. $item->id .'/edit') }}" title="Ubah Data"><button type='button' class='btn btn-default'><i class='fa fa-edit'></i></button></a>
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            {{ Form::close() }}
                        </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->jenis=='admin')
            <a href="{{ url('/perjanjian/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
