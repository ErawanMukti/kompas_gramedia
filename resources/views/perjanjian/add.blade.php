@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Area</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'perjanjian', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('no_perjanjian') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">No Perjanjian :</label>

                <div class="col-md-6">
                    <input id="no_perjanjian" type="text" class="form-control" name="no_perjanjian" value="{{ old('no_perjanjian') }}">

                    @if ($errors->has('no_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nama_perjanjian') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Nama Perjanjian :</label>

                <div class="col-md-6">
                    <input id="nama_perjanjian" type="text" class="form-control" name="nama_perjanjian" value="{{ old('nama_perjanjian') }}">

                    @if ($errors->has('nama_perjanjian'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_perjanjian') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('isi_pasal') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Isi Pasal :</label>

                <div class="col-md-6">
                    <input id="isi_pasal" type="file" class="form-control" name="isi_pasal" value="{{ old('isi_pasal') }}">

                    @if ($errors->has('isi_pasal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('isi_pasal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
