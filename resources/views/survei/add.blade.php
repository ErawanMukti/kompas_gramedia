@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Buat Jadwal Survei Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'survei', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}
            
            <div class="form-group{{ $errors->has('id_ruangan') ? ' has-error' : '' }}">
                <label for="id_ruangan" class="col-md-4 control-label">Pilih Ruangan :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_ruangan">
                        @foreach($ruangan as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_ruangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_ruangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('tanggal_survei') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Tanggal Survei :</label>

                <div class="col-md-6">
                    <input id="tanggal_survei" type="date" class="form-control" name="tanggal_survei" value="{{ old('tanggal_survei') }}">

                    @if ($errors->has('tanggal_survei'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_survei') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('waktu_survei') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Waktu Survei :</label>

                <div class="col-md-6">
                    <input id="waktu_survei" type="time" class="form-control" name="waktu_survei" value="{{ old('waktu_survei') }}">

                    @if ($errors->has('waktu_survei'))
                        <span class="help-block">
                            <strong>{{ $errors->first('waktu_survei') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
