@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Daftar Janji Survei Ruangan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>Tanggal Survei</td>
                    <td>Waktu Survei</td>
                    <td>Ruangan</td>
                    <td>Status</td>
                    <td width="60">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($survei as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ date('d/m/Y', strtotime($item->tanggal_survei)) }}</td>
                    <td>{{ $item->waktu_survei }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->id_users == null ? 'Available' : 'Booked by. ' . $item->email }}</td>
                    <td>
                        {{ Form::open(array('url' => 'survei/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            @if ( Auth::user()->jenis != 'admin' && $item->id_users == null )
                                <a href="{{ url('survei/'. $item->id .'/book') }}" title="Booking Jadwal"><button type='button' class='btn btn-default'><i class='fa fa-book'></i></button></a>
                            @elseif(Auth::user()->jenis=='admin' )
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->jenis=='admin' )
            <a href="{{ url('/survei/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
