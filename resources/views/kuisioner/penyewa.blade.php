@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Gedung Perkantoran Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Pertanyaan</td>
                                <td>Section</td>
                                <td>Status</td>
                                <!--<td>Action</td>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kuisioner as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->pertanyaan }}</td>
                                <td>{{ $item->id_section}}</td>
                                <td>{{ $item->status }}</td>
                                <!--<td>
                                    <form method="POST" action="{{ url('kuisioner/destroy', $item->id) }}" accept-charset="UTF-8">
                                    <a class="btn btn-xs btn-info" href="{{ url('kuisioner/edit', $item->id) }}">Ubah</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>

                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='pimpinan')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Pertanyaan</td>
                                <td>Section</td>
                                <td>Status</td>
                        </thead>
                        <tbody>
                            @foreach($kuisioner as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->pertanyaan }}</td>
                                <td>{{ $item->section}}</td>
                                <td>{{ $item->status }}</td>
                           </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <!--<a href="{{ url('/kuisioner/add') }}" class="btn btn-info">Tambah</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
