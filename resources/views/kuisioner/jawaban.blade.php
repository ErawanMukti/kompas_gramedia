@extends('layouts.app')

@section('content')
<html>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Lembar Jawaban Kuisioner</div>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/kuisioner/jawabanStore') }}">
                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                <table id="example1" class="table table-bordered table-striped">
                            <label for="id_area" class="col-md-4 control-label">Pilih Kontrak :</label>

                            <div class="col-md-6">
                            <select class="form-control" name="id_kontrak">
                            @foreach($kontrak as $item)
                                    <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                                </select>
                            </div>

                        <thead>
                            <tr><?php $i=1; ?>
                            @foreach($section as $item)
                                <td>Section : {{$item->section}}</td>
                                <td>Sangat Tidak Puas</td>
                                <td>Tidak Puas</td>
                                <td>Biasa</td>
                                <td>Puas </td>
                                <td>Sangat Puas</td>
                            </tr>
                        </thead>
                        <br>    
                        <tbody>
                            <tr>
                            <?php $kuisioner=$item->kuisionerKepuasan; ?>
                                 @foreach($kuisioner as $item)
                                <td>{{ $item->pertanyaan }}</td>
                                <td><input name='jawaban{{$i}}' value='1' type="radio"></td>
                                <td><input name='jawaban{{$i}}' value='2' type="radio"></td>
                                <td><input name='jawaban{{$i}}' value='3' type="radio"></td>
                                <td><input name='jawaban{{$i}}' value='4' type="radio"></td>
                                <td><input name='jawaban{{$i}}' value='5' type="radio"></td>
                                <?php $i++; ?>
                                @endforeach
                                <br>
                            </tr>
                            @endforeach
                        </tbody>
                        <input type ="hidden" name="index" value="{{$i}}">

                    </table>

                   <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Masukkan Data
                                </button>
                            </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<html>
@endsection
