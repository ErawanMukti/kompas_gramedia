@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Kuisioner Kepuasan Pelanggan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Section</td>
                    <td>Pertanyaan</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($kuisioner as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->kategori }}</td>
                    <td>{{ $item->pertanyaan }}</td>
                    <td>
                        {{ Form::open(array('url' => 'kuisioner/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('kuisioner/'. $item->id .'/edit') }}" title="Ubah Data"><button type='button' class='btn btn-default'><i class='fa fa-edit'></i></button></a>
                            {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ url('/kuisioner/create') }}" class="btn btn-info">Tambah</a>
    </div>
    <!-- /.box-body -->
</div>
@endsection
