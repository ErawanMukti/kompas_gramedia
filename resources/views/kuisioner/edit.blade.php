@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Data Pertanyaan Kuisioner</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($kuisioner, array('route' => array('kuisioner.update', $kuisioner->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('pertanyaan') ? ' has-error' : '' }}">
                {{ Form::label('pertanyaan', 'Pertanyaan', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('pertanyaan', null, array('class' => 'form-control') )}}

                    @if ($errors->has('pertanyaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pertanyaan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_section') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Section :</label>

                <div class="col-md-6">
                    {{ Form::select('id_section', $section, $kuisioner->id_section, array('id'=>'id_section', 'class' => 'form-control')) }}

                    @if ($errors->has('id_section'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_section') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {!! form::close() !!}
    </div>
</div>
@endsection
