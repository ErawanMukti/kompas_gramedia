@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Hasil Kuisioner</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'kuesioner/grafik', 'method' => 'POST', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal :</label>

                <div class="col-md-6">
                    <input id="tanggal" type="month" class="form-control" name="tanggal" value="{{ date('Y-m') }}" >

                    @if ($errors->has('tanggal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_section') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Section :</label>

                <div class="col-md-6">
                    {{ Form::select('id_section', $section, null, array('id'=>'id_section', 'class' => 'form-control')) }}
                </div>
            </div>
                     
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Lihat Hasil Kuisioner
                    </button>
                </div>
            </div>
        {{ Form::close() }}            
    </div>
</div>
@if ( $grafik == true )
    <div class="row">
        <?php
            foreach ($data as $key => $value) {
                echo '
                <div class="col-xs-6">
                    <!-- Donut chart -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>

                            <h3 class="box-title">'. $value['nama'] .'</h3>
                        </div>
                        <div class="box-body">
                            <div id="donut-'. $value['id'] .'" style="height: 300px;"></div>
                        </div>
                        <!-- /.box-body-->
                    </div>
                    <!-- /.box -->
                </div>';
            }
        ?>
    </div>
@endif
@endsection
@section('js_content')
<script>
    @if ( $grafik == true )
        $(function () {
            <?php foreach ($data as $key => $value) { ?>
                var donutData<?php echo $value['id']; ?> = [
                    <?php foreach ($value['jawaban'] as $k_jawaban => $v_jawaban) {
                        echo "{label: 'Nilai: $v_jawaban->jawaban' , data: $v_jawaban->jumlah},";
                    } ?>
                ];
                $.plot("#donut-<?php echo $value['id']; ?>", donutData<?php echo $value['id']; ?>, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                formatter: labelFormatter,
                                threshold: 0.1
                            }
                        }
                    },
                    legend: {
                        show: false
                    }
                });
            <?php } ?>
        });
    @endif
    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
            + label
            + "<br>"
            + Math.round(series.percent) + "%</div>";
    }
</script>
@endsection
