@extends('layouts.lte')

@section('content')
{{ Form::open(array('url' => 'kuesioner/isi', 'method' => 'POST', 'class' => 'form-horizontal')) }}
    {{ csrf_field() }}
    
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">Tambah Pertanyaan Kuisioner</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(Session::has('alert-success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                    {{ Session::get('alert-success') }}
                </div>
            @endif

            @if(Session::has('alert-danger'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Terjadi Kesalahan!</h4>
                    {{ Session::get('alert-danger') }}
                </div>
            @endif

            <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal :</label>

                <div class="col-md-6">
                    <input id="tanggal" type="date" class="form-control" name="tanggal" value="{{ date('Y-m-d') }}" >

                    @if ($errors->has('tanggal'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_kontrak') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">No Kontrak :</label>

                <div class="col-md-6">
                    {{ Form::select('id_kontrak', $kontrak, null, array('id'=>'id_kontrak', 'class' => 'form-control')) }}

                    @if ($errors->has('id_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
                     
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        </div>
    </div>

    @foreach ( $pertanyaan as $key => $value )
        <?php echo $value; ?>
    @endforeach
{{ Form::close() }}
@endsection
