@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Pertanyaan Kuisioner</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'kuisioner', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}
                      
            <div class="form-group{{ $errors->has('pertanyaan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Pertanyaan :</label>

                <div class="col-md-6">
                    <input id="pertanyaan" type="text" class="form-control" name="pertanyaan" value="{{ old('pertanyaan') }}">

                    @if ($errors->has('pertanyaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pertanyaan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('id_section') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Section :</label>

                <div class="col-md-6">
                    {{ Form::select('id_section', $section, null, array('id'=>'id_section', 'class' => 'form-control')) }}

                    @if ($errors->has('id_section'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_section') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
                     
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
