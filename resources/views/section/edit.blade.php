@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Data Section</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($section, array('route' => array('section.update', $section->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
                {{ Form::label('kategori', 'Kategori', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('kategori', null, array('class' => 'form-control') )}}

                    @if ($errors->has('kategori'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kategori') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {!! form::close() !!}
    </div>
</div>
@endsection
