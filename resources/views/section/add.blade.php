@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Section</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'section', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Kategori :</label>

                <div class="col-md-6">
                    <input id=kategori type="text" class="form-control" name="kategori" value="{{ old('section') }}">

                    @if ($errors->has('kategori'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kategori') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
