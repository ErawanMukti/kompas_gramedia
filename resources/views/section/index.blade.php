@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Kuisioner Kepuasan Pelanggan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td width="40">ID</td>
                    <td>Section</td>
                    <td width="100">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($section as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->kategori }}</td>
                    <td>
w                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ url('/section/create') }}" class="btn btn-info">Tambah</a>
    </div>
    <!-- /.box-body -->
</div>
@endsection
