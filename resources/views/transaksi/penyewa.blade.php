@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Transaksi Persewaan</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Total Bayar</td>
                                <td>Tanggal Jatuh Tempo</td>
                                <td>Tanggal Bayar</td>
                                <!--<td>ID Kontrak yang Terlibat</td>-->
                                <!--<td>Action</td>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transaksi as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->total_bayar }}</td>
                                <td>{{ $item->tanggal_jatuh_tempo }}</td>
                                <td>{{ $item->tanggal_bayar }}</td>
                                <!--<td>{{ $item->id_kontrak }}</td>-->
                                <!--<td>
                                    <form method="POST" action="{{ url('transaksi/destroy', $item->id) }}" accept-charset="UTF-8">
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <a href="{{ url('/transaksi/add') }}" class="btn btn-info">Tambah</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
