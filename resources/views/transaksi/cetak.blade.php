@extends('layouts.cetak')

@section('content')
    <h3>Bukti Transaksi Persewaan Gedung Kompas Gramedia</h3>
    <table class="table table-bordered table-striped">
        <tr>
            <td width="200">No Transaksi</td>
            <td>{{ $transaksi->id }}</td>   
        </tr>
        <tr>
            <td>Tanggal Pembayaran</td>
            <td>{{ date('d/m/Y', strtotime($transaksi->tanggal_bayar)) }}</td>   
        </tr>
        <tr>
            <td>No Kontrak</td>
            <td>{{ $transaksi->id_kontrak }}</td>
        </tr>
        <tr>
            <td>Jenis Pembayaran</td>
            <td>{{ $transaksi->jenis_bayar }}</td>
        </tr>
        <tr>
            <td>Jumlah Pembayaran</td>
            <td>{{ number_format($transaksi->total_bayar) }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>{{ $transaksi->keterangan }}</td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="75%"></td>
            <td>
                <center>
                    <p>Surabaya, {{ date('d M Y') }}</p>
                    <p><img src="{{ asset('assets/img/logo.png') }}" style="width: 100%; height: auto;"></p>
                    <p>Manajemen Pergedungan Kompas Media</p>
                </center>
            </td>
        </tr>
    </table>

@endsection