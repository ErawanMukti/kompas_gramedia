@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ruang Kantor Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                     @foreach($transaksi as $item)
                    <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <td>Total Bayar</td>
                                <td>{{ $item->total_bayar }}</td>
                            </tr>

                            <tr>
                                <td>Tanggal Jatuh Tempo</td>
                                <td>{{ $item->tanggal_jatuh_tempo }} tahun</td>
                            </tr>

                            <tr>
                                <td>Tanggal Bayar</td>
                                <td>{{ $item->tanggal_bayar }}</td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
