@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Data Transaksi</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'transaksi', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('id_kontrak') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">ID Kontrak :</label>

                <div class="col-md-6">
                    {{ Form::select('id_kontrak', $kontrak, null, array('id'=>'id_kontrak', 'class' => 'form-control')) }}

                    @if ($errors->has('id_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('jenis_bayar') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Jenis Bayar :</label>

                <div class="col-md-6">
                    {{
                        Form::select('jenis_bayar', array(
                            'Deposit'                 => 'Deposit',
                            'Biaya Overtime AC'       => 'Biaya Overtime AC',
                            'Pembayaran PLN'          => 'Pembayaran PLN',
                            'Biaya Cleaning Service'  => 'Biaya Cleaning Service',
                            'Pembayaran Sewa Ruangan' => 'Pembayaran Sewa Ruangan',
                            'Biaya Reklame'           => 'Biaya Reklame',
                            'Denda Pengosongan'       => 'Denda Pengosongan',
                            'Lain-Lain'               => 'Lain-Lain'
                        ), Input::old('jenis_bayar'), array('class' => 'form-control'))
                    }}

                    @if ($errors->has('jenis_bayar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('jenis_bayar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('total_bayar') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Total Bayar :</label>

                <div class="col-md-6">
                    <input id="total_bayar" type="text" class="form-control" name="total_bayar" value="{{ old('total_bayar') }}">

                    @if ($errors->has('total_bayar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('total_bayar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Keterangan :</label>

                <div class="col-md-6">
                    <textarea id="keterangan" class="form-control" name="keterangan"></textarea>

                    @if ($errors->has('keterangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('keterangan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_bayar') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Bayar :</label>

                <div class="col-md-6">
                    <input id="tanggal_bayar" type="date" class="form-control" name="tanggal_bayar" value="{{ date('Y-m-d') }}">

                    @if ($errors->has('tanggal_bayar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_bayar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
