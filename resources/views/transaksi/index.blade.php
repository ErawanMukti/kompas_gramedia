@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Transaksi Persewaan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>ID Kontrak yang Terlibat</td>
                    <td>Jenis Bayar</td>
                    <td>Total Bayar</td>
                    <td>Tanggal Bayar</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($transaksi as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->id_kontrak }}</td>
                    <td>{{ $item->jenis_bayar }}</td>
                    <td>{{ number_format($item->total_bayar) }}</td>
                    <td>{{ date('d/m/Y', strtotime($item->tanggal_bayar)) }}</td>
                    <td>
                        {{ Form::open(array('url' => 'transaksi/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('transaksi/'. $item->id .'/cetak') }}" title="Cetak Transaksi" target="_blank"><button type='button' class='btn btn-default'><i class='fa fa-print'></i></button></a>
                            @if ( Auth::user()->jenis == 'admin' )
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if ( Auth::user()->jenis == 'admin' )
            <a href="{{ url('/transaksi/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
