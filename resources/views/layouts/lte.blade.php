<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Kompas Gramedia || Persewaan Kantor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets/css/lte/ionicons.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/css/lte/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/css/lte/skins/_all-skins.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>KG</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ asset('assets/img/logo.png') }}"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('assets/img/lte/boxed-bg.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->nama }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('assets/img/lte/boxed-bg.jpg') }}" class="img-circle" alt="User Image">
                <p>
                  {{ Auth::user()->nama }}
                  <small>{{ Auth::user()->jenis }}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        @if( Auth::user()->jenis != 'surveyor' )
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Informasi Space Kantor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if( Auth::user()->jenis == 'admin' )
              <li><a href="{{ url('area/index') }}"><i class="fa fa-circle-o"></i> Area</a></li>
              <li><a href="{{ url('gedung/index') }}"><i class="fa fa-circle-o"></i> Gedung</a></li>
            @endif
            <li><a href="{{ url('ruangan/index') }}"><i class="fa fa-circle-o"></i> Ruangan</a></li>
          </ul>
        </li>
        @endif
        <li class="treeview">
          <a href="{{ url('survei/index') }}"><i class="fa fa-edit"></i> <span>Survei</span></a>
        </li>
        <li class="treeview">
          <a href="{{ url('saran/index') }}"><i class="fa fa-edit"></i> <span>Saran</span></a>
        </li>
        @if( Auth::user()->jenis != 'surveyor' )
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Dokumentasi Sewa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('perjanjian/index') }}"><i class="fa fa-circle-o"></i> Perjanjian</a></li>
            <li><a href="{{ url('kontrak/index') }}"><i class="fa fa-circle-o"></i> Kontrak</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-sticky-note"></i>
            <span>Permintaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('permintaan/index') }}"><i class="fa fa-circle-o"></i> Daftar Permintaan</a></li>
            @if( Auth::user()->jenis == 'admin' )
              <li><a href="{{ url('laporan/permintaan') }}"><i class="fa fa-circle-o"></i> Laporan Permintaan</a></li>
            @endif
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Kuisioner</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if( Auth::user()->jenis == 'admin' )
              <li><a href="{{ url('section/index') }}"><i class="fa fa-circle-o"></i> Section</a></li>
              <li><a href="{{ url('kuisioner/index') }}"><i class="fa fa-circle-o"></i> Data Pertanyaan</a></li>
              <li><a href="{{ url('kuesioner/hasil') }}"><i class="fa fa-circle-o"></i> Hasil Kuisioner</a></li>
            @else
              <li><a href="{{ url('kuesioner/isi') }}"><i class="fa fa-circle-o"></i> Isi Kuisioner</a></li>
            @endif
          </ul>
        </li>
        <li class="treeview">
          <a href="{{ url('komplain/index') }}"><i class="fa fa-edit"></i> <span>Komplain</span></a>
        </li>
        @if( Auth::user()->jenis == 'admin' )
        <li class="treeview">
          <a href="{{ url('user/index') }}"><i class="fa fa-user"></i> <span>Data Penyewa</span></a>
        </li>
        @endif
        <li class="treeview">
          <a href="{{ url('transaksi/index') }}"><i class="fa fa-cart-plus"></i> <span>Transaksi</span></a>
        </li>
        @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $title }}
        <small>{{ $desc }}</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; 2017, Yovita. Designed by <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/lte/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/lte/demo.js') }}"></script>
<!-- FLOT CHARTS -->
<script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="{{ asset('assets/plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

@yield('js_content')
</body>
</html>
