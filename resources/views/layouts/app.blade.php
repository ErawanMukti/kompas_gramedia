<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KG || Persewaan Kantor</title>

    <!-- Fonts -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                @if(Auth::guest())
                <a class="navbar-brand" href="{{!! asset('assets/img/logo') !!}}">
                    Kompas  Gramedia
                </a>

                @elseif(Auth::user()->jenis=='admin')
                <a class="navbar-brand"> KG || Admin</a>

                @elseif(Auth::user()->jenis=='pimpinan')
                <a class="navbar-brand">KG || Pimpinan</a>

                @elseif(Auth::user()->jenis=='penyewa')
                <a class="navbar-brand">KG || Penyewa</a>

                 @elseif(Auth::user()->jenis=='surveyor')
                <a class="navbar-brand">KG || Penyewa</a>
                @endif
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if(Auth::guest())
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                </ul>

                @elseif(Auth::user()->jenis=='admin')
                  <ul class="nav navbar-nav">
                 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Informasi Space Kantor <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('area/index') }}" tabindex="-1" ><i class="fa fa-btn "></i>Area</a></li>
                        <li><a href="{{ url('gedung/index') }}"><i class="fa fa-btn "></i>Gedung</a></li>
                         <li><a href="{{ url('ruangan/index') }}" tabindex="-1" ><i class="fa fa-btn "></i>Ruangan</a></li>
                        </ul>
                </li>
                 </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('survei/index') }}">Survei</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('perjanjian/index') }}">Perjanjian</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('permintaan/index') }}">Permintaan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('kontrak/index') }}">Kontrak</a></li>
                </ul>
               
                <ul class="nav navbar-nav">
                 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Kusioner <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('section/index') }}" tabindex="-1" ><i class="fa fa-btn "></i>Section</a></li>
                        <li><a href="{{ url('kuisioner/index') }}"><i class="fa fa-btn "></i>Data Pertanyaan</a></li>
                         <li><a href="{{ url('') }}" tabindex="-1" ><i class="fa fa-btn "></i>Hasil Kuisioner</a></li>
                        </ul>
                </li>
                 </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('komplain/index') }}">Komplain</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('transaksi/index') }}">Transaksi</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('user/index') }}"> Data Penyewa</a></li>
                </ul>

                @elseif(Auth::user()->jenis=='pimpinan')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('area/index') }}">Area</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('gedung/index') }}">Gedung</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('ruangan/index') }}">Ruangan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('survei/index') }}">Survei</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('perjanjian/index') }}">Perjanjian</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('kontrak/index') }}">Kontrak</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Kuisioner Kepuasan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Komplain</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('transaksi/index') }}">Transaksi</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('user/index') }}"> Data User</a></li>
                </ul>


                @elseif(Auth::user()->jenis=='penyewa')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('ruangan/publicpenyewa') }}">Ruangan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('perjanjian/penyewa') }}">Perjanjian</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('kontrak/penyewa') }}">Kontrak</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('permintaan/index') }}">Permintaan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('kuisioner/penyewa') }}">Kuisioner Kepuasan</a></li>
                </ul>

                <ul class="nav navbar-nav">
                 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Komplain <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('komplain/add') }}" tabindex="-1" ><i class="fa fa-btn "></i>Tambah Komplain</a></li>
                        <li><a href="{{ url('komplain/penyewa') }}"><i class="fa fa-btn "></i>Detail Komplain</a></li>
                        </ul>
                </li>
                </ul>

               <ul class="nav navbar-nav">
                    <li><a href="{{ url('transaksi/penyewa') }}">Transaksi</a></li>
                </ul>

                @elseif(Auth::user()->jenis=='surveyor')
                 <ul class="nav navbar-nav">
                 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data Survei<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('survei/add') }}" tabindex="-1" ><i class="fa fa-btn "></i>Isi Form Survei</a></li>
                        <li><a href="{{ url('survei/index') }}"><i class="fa fa-btn "></i>Data Survei</a></li>
                        </ul>
                </li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="{{ url('saran/index') }}">Saran</a></li>
                </ul>
                @endif
                
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('user/add') }}">Register</a></li>
        
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->nama }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <link href="{{ asset('assets/js/jquery.min.js') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" rel="stylesheet">
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
