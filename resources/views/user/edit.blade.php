@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ubah Data Penyewa</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('nama', 'Nama', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('nama', null, array('class' => 'form-control') )}}

                    @if ($errors->has('nama'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Password', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::password('password', array('class' => 'form-control') )}}

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('institusi', 'Institusi', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('institusi', null, array('class' => 'form-control') )}}

                    @if ($errors->has('institusi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('institusi') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('no_telp', 'No Telepon', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}

                <div class="col-md-6">
                    {{ Form::text('no_telp', null, array('class' => 'form-control') )}}

                    @if ($errors->has('no_telp'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_telp') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Simpan', array('class' => 'btn btn-info'))}}
                </div>
            </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>
@endsection
