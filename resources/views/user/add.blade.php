@extends('layouts.lte-logreg')

@section('content')
    <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}"></a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Register User</p>

        <form method="POST" action="{{ url('user/store') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}" placeholder="Nama User">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>

                @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="institusi" type="text" class="form-control" name="institusi" value="{{ old('institusi') }}" placeholder="Institusi">
                <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>

                @if ($errors->has('institusi'))
                    <span class="help-block">
                        <strong>{{ $errors->first('institusi') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="no_telp" type="text" class="form-control" name="no_telp" value="{{ old('no_telp') }}" placeholder="No. Telepon">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>

                @if ($errors->has('no_telp'))
                    <span class="help-block">
                        <strong>{{ $errors->first('no_telp') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-8">&nbsp;</div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <input type="hidden" name="jenis" value="surveyor">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="{{ url('/login') }}" class="text-center">Saya Sudah Terdaftar</a>
      </div>
      <!-- /.form-box -->
    </div>
@endsection
