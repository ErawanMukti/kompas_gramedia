@extends('layouts.lte')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Notifikasi</div>

                <div class="panel-body">
                    @if ( count($kontrak) > 0 )
                        Anda memiliki kontrak baru dengan nomor <strong>{{ $kontrak[0]->no_kontrak }}</strong>
                    @endif

                    @if ( count($transaksi) > 0 )
                        Anda memiliki transaksi baru dengan nomor <strong>{{ $transaksi[0]->id }}</strong> pada tanggal <strong>{{ $transaksi->tanggal_bayar }}</strong>
                    @endif

                    @if ( count($transaksi) == 0 && count($kontrak) == 0 )
                        Anda tidak memiliki transaksi atau kontrak baru
                    @endif                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
