@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tambah Komplain</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {{ Form::open(array('url' => 'komplain', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('subjek') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Subject Komplain</label>

                <div class="col-md-6">
                    <input id="subjek" type="text" class="form-control" name="subjek" value="{{ old('subjek') }}">

                    @if ($errors->has('subjek'))
                        <span class="help-block">
                            <strong>{{ $errors->first('subjek') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('isi_komplain') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Isi Komplain</label>

                <div class="col-md-6">
                    <textarea class="form-control" name="isi_komplain" value="{{old('isi_komplain')}}" placeholder="Masukkan keluhan Anda disini..."></textarea>

                    @if ($errors->has('isi_komplain'))
                        <span class="help-block">
                            <strong>{{ $errors->first('isi_komplain') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('tanggal_komplain') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">Tanggal Komplain</label>

                <div class="col-md-6">
                    <input class="form-control" type="date" name="tanggal_komplain" value="{{ date('Y-m-d') }}">

                    @if ($errors->has('tanggal_komplain'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_komplain') }}</strong>
                        </span>
                    @endif
                </div>
            </div>                        

            <div class="form-group{{ $errors->has('id_kontrak') ? ' has-error' : '' }}">
                <label for="id_kontrak" class="col-md-3 control-label">No Kontrak :</label>

                <div class="col-md-6">
                    <select class="form-control" name="id_kontrak">
                        @foreach($kontrak as $item)
                            <option value="{{$item->id}}">{{ $item->no_kontrak }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('id_kontrak'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id_kontrak') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                    <button type="submit" class="btn btn-primary">
                        Masukkan Data
                    </button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
