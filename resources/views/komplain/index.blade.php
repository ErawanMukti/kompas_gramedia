@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Komplain User</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                {{ Session::get('alert-success') }}
            </div>
        @endif
        @if( $block == true )
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4>    <i class="icon fa fa-check"></i> Perhatian!</h4>
                Silahkan melakukan pembayaran bulan ini untuk dapat menggunakan fasilitas ini
            </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>Subjek Komplain</td>
                    <td>Isi Komplain</td>
                    <td>Status</td>
                    <td width="100">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($komplain as $item)
                <tr>
                    <td>{{ $item->subjek }}</td>
                    <td>{{ $item->isi_komplain }}</td>
                    @if ( $item->status == 0 )
                        <td>Proses</td>
                    @elseif ( $item->status == 1 )
                        <td>Selesai</td>
                    @endif
                    <td>
                        {{ Form::open(array('url' => 'komplain/'.$item->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <a href="{{ url('komplain/'. $item->id .'/balas') }}" title="Detail Komplain"><button type='button' class='btn btn-default'><i class='fa fa-search'></i></button></a>
                            @if(Auth::user()->jenis == 'admin')
                                <a href="{{ url('komplain/'. $item->id .'/accept') }}" title="Komplain Selesai"><button type='button' class='btn btn-default'><i class='fa fa-check'></i></button></a>
                            @endif
                            @if(Auth::user()->jenis == 'penyewa')
                                {{ Form::button('', array('type' => 'submit', 'class' => 'btn btn-danger fa fa-trash', 'onclick' => "return confirm('Anda yakin akan menghapus data ?');")) }}
                            @endif
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if( Auth::user()->jenis == 'penyewa' && $block == false)
            <a href="{{ url('/komplain/create') }}" class="btn btn-info">Tambah</a>
        @endif
    </div>
    <!-- /.box-body -->
</div>
@endsection
