@extends('layouts.lte')

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Detail Komplain User</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @foreach($komplain as $item)
            <table id="example1" class="table table-bordered table-striped">
                <tr>
                    <td>Jam dan Tanggal</td>
                    <td>{{$item->created_at}}</td>
                </tr>

                <tr>
                    <td>Subjek Komplain</td>
                    <td>{{$item->subjek}}</td>
                </tr>

                <tr>
                    <td>No Kontrak</td>
                    <td>{{$kontrak->no_kontrak}}</td>
                </tr>

                <tr>
                    <td>Nama Ruangan</td>
                    <td>{{$ruangan->nama}}</td>
                </tr>

                <tr>
                    <td>Isi Komplain</td>
                    <td>{{$item->isi_komplain}}</td>
                </tr>
            </table>
        @endforeach

        <br/><strong>Komentar Admin</strong>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>Tanggal</td>
                    <td>Pesan</td>
                </tr>
            </thead>
            <tbody>
                @foreach($balas as $item)
                <tr>
                    <td>{{ $item->tgl_balas }}</td>
                    <td>{{ $item->pesan }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <br/>
        @if(Auth::user()->jenis=='admin')
            {{ Form::open(array('url' => 'komplain/balas', 'method' => 'POST')) }}
                <div class="form-group{{ $errors->has('pesan') ? ' has-error' : '' }}">
                    <textarea class="form-control" placeholder="Tulis Komentar" name="pesan"></textarea>

                    @if ($errors->has('pesan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pesan') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div>
                        <input type="hidden" name="id_komplain" value="{{ $id }}">
                        <button type="submit" class="btn btn-primary">
                            Balas Komplain
                        </button>
                    </div>
                </div>
            {!! form::close() !!}
        @endif
    </div>
</div>
@endsection
