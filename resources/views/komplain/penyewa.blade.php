@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Area Kantor Kompas Gramedia</div>

                <div class="panel-body">
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>    <i class="icon fa fa-check"></i> Berhasil!</h4>
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>Subjek Komplain</td>
                                <td>Isi Komplain</td>
                                <td>Status</td>
                                <!--<td>Action</td>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($komplain as $item)
                            <tr>
                                <td>{{ $item->subjek }}</td>
                                <td>{{ $item->isi_komplain }}</td>
                                <td>{{ $item->status }}</td>
                                <!--<td>
                                    <form method="POST" action="{{ url('area/destroy', $item->id) }}" accept-charset="UTF-8">
                                    <a class="btn btn-xs btn-info" href="{{ url('komplain/balas', $item->id) }}">balas</a>
                                    <input class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" value="Hapus" />
                                    </form>
                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @elseif(Auth::user()->jenis=='pimpinan')
                       <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>Subjek Komplain</td>
                                <td>Isi Komplain</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($komplain as $item)
                            <tr>
                                <td>{{ $item->subjek }}</td>
                                <td>{{ $item->isi_komplain }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @endif

                    @if(Auth::user()->jenis=='penyewa')
                    <!--<a href="{{ url('/area/add') }}" class="btn btn-info">Tambah</a>-->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
