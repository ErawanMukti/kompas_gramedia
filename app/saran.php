<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saran extends Model
{
	protected $table = 'saran';
	protected $primaryKey = 'id';
	protected $fillable = ['id', 'isi_saran', 'tgl_saran', 'id_users', 'id_ruangan'];

	public function users()
    {
    	return $this->BelongsTo('App\users');
    }
    
}