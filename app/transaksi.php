<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $table = 'transaksis';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'total_bayar', 'tanggal_jatuh_tempo', 'tanggal_bayar', 'id_kontrak', 'jenis_bayar', 'keterangan'];

    public function kontrak()
    {
    	return $this->BelongsTo('App\kontrak');
    }
}
