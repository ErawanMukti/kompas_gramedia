<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\kuisionerKepuasan;
use App\section;
use App\isi;
use App\kontrak;
use Auth, Validator, Input, Redirect;

class kuisionerController extends Controller
{
    private $title = "Data Pertanyaan Kuisioner";
    private $desc  = "Manajemen data pertanyaan kuisioner";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $kuisioner = DB::table('kuisioner_kepuasans')
                 ->join('sections', 'kuisioner_kepuasans.id_section', '=', 'sections.id')
                 ->select('kuisioner_kepuasans.*', 'sections.kategori')
                 ->get();

    	return view('kuisioner.index', array(
            'title'     => $this->title,
            'desc'      => $this->desc,
            'kuisioner' => $kuisioner
        ));
    }

    public function create()
    {
        $section = section::pluck('kategori', 'id');
        return view('kuisioner.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'section' => $section
        ));
    }

    public function store(Request $request)
    {
        $rules = array( 'pertanyaan' => 'required' );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("kuisioner/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$kuisioner = kuisionerKepuasan::create($request->all());
    	$kuisioner->save();
    	return redirect('kuisioner/index');
    }

    public function edit($id)
    {
        $kuisioner = kuisionerKepuasan::findOrFail($id);
        $section = section::pluck('kategori', 'id');
        return view('kuisioner.edit', array(
            'title'     => $this->title,
            'desc'      => $this->desc,
            'kuisioner' => $kuisioner,
            'section'   => $section
        ));
    }

    public function update(Request $request,$id)
    {
        $rules = array( 'pertanyaan' => 'required' );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("kuisioner/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $kuisioner = kuisionerKepuasan::whereId($id)->first();
        $kuisioner->pertanyaan = $request->get('pertanyaan');
        $kuisioner->id_section = $request->get('id_section');
        $kuisioner->save();
        return redirect('kuisioner/index');
    }

    public function destroy($id)
    {
        kuisionerKepuasan::destroy($id);
        return back()->with('alert_success','');
    }

    public function isi()
    {
        $section = section::all();
        $kontrak = kontrak::where('id_users', '=', Auth::user()->id)->pluck('no_kontrak', 'id');

        $h_section = array();
        foreach ($section as $value) {
            array_push($h_section, $this->html_isi($value));
        }
        return view('kuisioner.isi', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'kontrak'    => $kontrak,
            'pertanyaan' => $h_section
        ));
    }

    public function html_isi($data)
    {
        $html = '
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kategori '. $data->kategori .'</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> '. $this->html_pertanyaan($data) .'
            </div>
        </div>';
        return $html;
    }

    public function html_pertanyaan($data)
    {
        $pertanyaan = kuisionerKepuasan::where('id_section', '=', $data->id)->get();

        $html = '';
        foreach ($pertanyaan as $value) {
            $html .= '
                <div class="form-group"> 
                    <label for="name" class="col-md-5 control-label">'. $value->pertanyaan .'</label>

                    <div class="col-md-2">
                        <select name="pertanyaan['. $value->id .']" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>';
        }
        return $html;
    }

    public function simpan_isi()
    {
        $tanggal = Input::get('tanggal');
        $kontrak = Input::get('id_kontrak');

        //pengecekan apakah di bulan ini sudah pernah isi survei atau belum
        $jawaban = DB::table('isis')
                 ->where('id_kontrak', '=', $kontrak)
                 ->whereMonth('tanggal', '=', date('m', strtotime($tanggal)))
                 ->whereYear('tanggal', '=', date('Y', strtotime($tanggal)))
                 ->get();
        if ( count($jawaban) > 0){
            return back()->with('alert-danger','Bulan ini anda sudah mengisi kuisioner untuk kontrak ini. Silahkan mengisi kembali bulan depan');
        }

        //jika belum, simpan data survei
        foreach (Input::get('pertanyaan') as $key => $value) {
            $isi = new isi();
            $isi->jawaban       = $value;
            $isi->id_kontrak    = $kontrak;
            $isi->id_pertanyaan = $key;
            $isi->tanggal       = $tanggal;
            $isi->save();
        }
        return back()->with('alert-success','');
    }

    public function hasil()
    {
        $section = section::pluck('kategori', 'id');
        $grafik  = false;
        return view('kuisioner.hasil', array(
            'title'   => 'Hasil Kuisioner',
            'desc'    => 'Menampilkan hasil kuisinoer per bulan',
            'section' => $section,
            'grafik'  => $grafik
        ));
    }

    public function grafik()
    {
        $section = section::pluck('kategori', 'id');
        $grafik  = true;

        $id_section = Input::get('id_section');
        $tanggal    = Input::get('tanggal');

        $pertanyaan = kuisionerKepuasan::where('id_section', '=', $id_section)->get();

        $arr_pertanyaan = array();
        foreach ($pertanyaan as $value) {
            $jawaban = DB::table('isis')
                     ->where('id_pertanyaan', '=', $value->id)
                     ->whereMonth('tanggal', '=', date('m', strtotime($tanggal)))
                     ->whereYear('tanggal', '=', date('Y', strtotime($tanggal)))
                     ->groupBy('jawaban')
                     ->select(DB::raw('jawaban, count(*) as jumlah'))
                     ->get();
            
            array_push($arr_pertanyaan, array('id'=>$value->id, 'nama'=>$value->pertanyaan, 'jawaban'=>$jawaban));
        }

        return view('kuisioner.hasil', array(
            'title'   => 'Hasil Kuisioner',
            'desc'    => 'Menampilkan hasil kuisinoer per bulan',
            'section' => $section,
            'grafik'  => $grafik,
            'data'    => $arr_pertanyaan
        ));
    }
}
