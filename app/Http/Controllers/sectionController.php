<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\section;

class sectionController extends Controller
{
    private $title = "Data Section Kuisioner";
    private $desc  = "Manajemen data section";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $section = section::all();
    	return view('section.index', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'section' => $section
        ));
    }

    public function create()
    {
        return view('section.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc
        ));
    }

    public function store(Request $request)
    {
    	$section = section::create($request->all());
    	$section->save();
    	return redirect('section/index');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_gedung' => 'required',
            'alamat_gedung' => 'required',
            'no_telp_gedung' => 'required',
        ]);
    }

    public function edit($id)
    {
        $section = section::findOrFail($id);
        return view('section.edit', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'section' => $section
        ));
    }

    public function update(Request $request,$id)
    {
        $section = section::whereId($id)->first();
        $section->kategori = $request->get('kategori');
        $section->save();
        return redirect('section/index');
    }

    public function destroy($id)
    {
        section::destroy($id);
        return back()->with('alert_success','');
    }

}
