<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\gedung;
use App\area;
use Validator, Input, Redirect;

class gedungController extends Controller
{
    private $title = "Data Gedung";
    private $desc  = "Manajemen data gedung";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $gedung = DB::table('gedungs')
             ->join('areas', 'gedungs.id_area', '=', 'areas.id')
             ->select('gedungs.*', 'areas.lokasi')
             ->get();

    	return view('gedung.index', array(
            'title'  => $this->title,
            'desc'   => $this->desc,
            'gedung' => $gedung
        ));
    }

    public function create()
    {
        $area = area::all();
        return view('gedung/add', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'area'  => $area
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'id_area'        => 'required',
            'nama_gedung'    => 'required',
            'alamat_gedung'  => 'required',
            'no_telp_gedung' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("gedung/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$gedung = gedung::create(Input::all());
    	$gedung->save();
    	return redirect('gedung/index');
    }

    public function edit($id)
    {
        $area   = area::all();
        $gedung = gedung::findOrFail($id);

        return view('gedung/edit', array(
            'title'  => $this->title,
            'desc'   => $this->desc,
            'area'   => $area,
            'gedung' => $gedung
        ));
    }

    public function update($id)
    {
        $rules = array(
            'nama_gedung'    => 'required',
            'alamat_gedung'  => 'required',
            'no_telp_gedung' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("gedung/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $gedung = gedung::find($id);
        $gedung->nama_gedung    = Input::get('nama_gedung');
        $gedung->alamat_gedung  = Input::get('alamat_gedung');
        $gedung->no_telp_gedung = Input::get('no_telp_gedung');
        $gedung->save();

        return redirect('gedung/index');
    }

    public function destroy($id)
    {
        gedung::destroy($id);
        return back()->with('alert_success','');
    }

}
