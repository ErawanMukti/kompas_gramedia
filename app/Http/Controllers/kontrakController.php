<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\kontrak;
use App\User;
use App\ruangan;
use App\perjanjian;
use Auth, Validator, Input, Redirect;

class kontrakController extends Controller
{
    private $title = "Data Kontrak";
    private $desc  = "Manajemen data kontrak";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        if ( Auth::user()->jenis == 'admin' ) {
            $kontrak = kontrak::all();
        } else {
            $kontrak = DB::table('kontraks')
                 ->where('id_users', '=', Auth::user()->id)
                 ->get();
        }
    	return view('kontrak.index', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'kontrak' => $kontrak
        ));
    }

    public function create()
    {
        $user    = User::all();
        $ruangan = DB::table('ruangans')
                 ->whereNotIn('id', function($qry) {
                    $qry->select('id_ruangan')
                        ->from('kontraks');
                 })
                 ->get();
        $perjanjian = DB::table('perjanjians')
                 ->whereNotIn('id', function($qry) {
                    $qry->select('id_perjanjian')
                        ->from('kontraks');
                 })
                 ->get();

    	return view('kontrak.add', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'user'       => $user,
            'ruangan'    => $ruangan,
            'perjanjian' => $perjanjian
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'no_kontrak'     => 'required',
            'id_users'       => 'required',
            'id_ruangan'     => 'required',
            'id_perjanjian'  => 'required',
            'file_kontrak'   => 'required',
            'jangka_waktu'   => 'required|numeric',
            'tanggal_masuk'  => 'required',
            'tanggal_keluar' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("kontrak/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$kontrak  = kontrak::create($request->all());
        $fileName = $request->file('file_kontrak')->getClientOriginalName();
        $request->file('file_kontrak')->move('fileKontrak/', $fileName);
        $kontrak->file_kontrak=$fileName;
    	$kontrak->save();

        $user = user::whereId($request->get('id_users'))->first();
        $user->jenis = 'penyewa';
        $user->save();
    	return redirect('kontrak/index');
    }

    public function detil(Request $request, $id)
    {
        $kontrak = DB::table('kontraks')
             ->join('perjanjians', 'kontraks.id_perjanjian', '=', 'perjanjians.id')
             ->join('ruangans', 'kontraks.id_ruangan', '=', 'ruangans.id')
             ->select('kontraks.*', 'perjanjians.nama_perjanjian', 'perjanjians.isi_pasal',
                      'ruangans.nama')
             ->where('kontraks.id', '=', $id)
             ->get();

        return view('kontrak.detil', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'kontrak' => $kontrak[0]
        ));
    }

    public function edit($id)
    {
        $kontrak    = kontrak::findOrFail($id);
        $perjanjian = perjanjian::all();

        return view('kontrak.edit', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'kontrak'    => $kontrak,
            'perjanjian' => $perjanjian
        ));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'no_kontrak'     => 'required',
            'jangka_waktu'   => 'required|numeric',
            'tanggal_masuk'  => 'required',
            'tanggal_keluar' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("kontrak/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $kontrak = kontrak::whereId($id)->first();
        $kontrak->no_kontrak     = $request->get('no_kontrak');
        $kontrak->jangka_waktu   = $request->get('jangka_waktu');
        $kontrak->tanggal_masuk  = $request->get('tanggal_masuk');
        $kontrak->tanggal_keluar = $request->get('tanggal_keluar');
        $kontrak->id_perjanjian  = $request->get('id_perjanjian');

        if ( $request->file('file_kontrak') != null ) {
            $fileName = $request->file('file_kontrak')->getClientOriginalName();
            $request->file('file_kontrak')->move('fileKontrak/', $fileName);
            $kontrak->file_kontrak=$fileName;
        }
        $kontrak->save();

        return redirect('kontrak/index');
    }

    public function destroy($id)
    {
        $kontrak = kontrak::whereId($id)->first();
        $id_user = $kontrak->id_users;

        kontrak::destroy($id);
 
        $user = user::whereId($id_user)->first();
        $user->jenis = 'surveyor';
        $user->save();
        return back()->with('alert-success','');
    }

}
