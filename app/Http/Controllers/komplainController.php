<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\komplain, App\kontrak, App\User, App\balas, App\ruangan;
use Auth, Input, Validator, Redirect;

class komplainController extends Controller
{
    private $title = "Data Komplain";
    private $desc  = "Manajemen data komplain";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $block = false;
        if ( Auth::user()->jenis == 'admin' ) {
            $komplain = komplain::all();
        } else {
            $jml_kontrak = kontrak::where('id_users', '=', Auth::user()->id)->count();

            $transaksi = DB::table('transaksis')
                        ->whereMonth('tanggal_bayar', '=', date('m'))
                        ->whereYear('tanggal_bayar', '=', date('Y'))
                        ->whereIn('id_kontrak', function($qry) {
                            $qry->select('id')
                                ->from('kontraks')
                                ->where('id_users', '=', Auth::user()->id);
                        })
                        ->whereIn('jenis_bayar', array('Biaya Overtime AC', 'Pembayaran PLN', 'Biaya Cleaning Service'))
                        ->get();
            $jml_transaksi = count($transaksi);

            //cek jumlah transaksi. dalam 1 bulan harus ada 3 transaksiL AC, PLN dan
            //Cleaning Service. Jika punya n kontrak maka (n x 3) transaksi
            if ( $jml_transaksi < ($jml_kontrak * 3) ) {
                $block = true;
            }

            $komplain = DB::table('komplains')
                 ->where('komplains.id_users', '=', Auth::user()->id)
                 ->get();
        }
    	return view('komplain.index', array(
            'title'    => $this->title,
            'desc'     => $this->desc,
            'komplain' => $komplain,
            'block'    => $block
        ));        
    }

    public function penyewa()
    {
        $komplain = komplain::all();
        return view('komplain.penyewa', compact('komplain'));
    }

    public function create()
    {
        $kontrak = DB::table('kontraks')
             ->where('kontraks.id_users', '=', Auth::user()->id)
             ->get();

        return view('komplain.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'kontrak' => $kontrak
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'subjek'           => 'required',
            'isi_komplain'     => 'required',
            'tanggal_komplain' => 'required',
            'id_kontrak'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("komplain/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$komplain = komplain::create($request->all());
    	$komplain->save();
    	return redirect('komplain/index');
    }

    public function balas($id)
    {
        $komplain = komplain::whereId($id)->get();
        $kontrak  = kontrak::whereId($komplain[0]->id_kontrak)->get();
        $ruangan  = ruangan::whereId($kontrak[0]->id_ruangan)->get();
        $balas    = DB::table('komplain_balas')
                    ->where('id_komplain', $komplain[0]->id)
                    ->get();

        return view('komplain.balas', array(
            'title'    => $this->title,
            'desc'     => $this->desc,
            'komplain' => $komplain,
            'id'       => $id,
            'balas'    => $balas,
            'ruangan'  => $ruangan[0],
            'kontrak'  => $kontrak[0]
        ));
    }

    public function simpanbalas()
    {
        $id_komplain = Input::get('id_komplain');
        $pesan       = Input::get('pesan');
        
        $rules = array(
            'pesan' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("komplain/$id_komplain/balas")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $balas = new balas;
        $balas->id_komplain = $id_komplain;
        $balas->id_users    = Auth::user()->id;
        $balas->tgl_balas   = date('Y-m-d');
        $balas->pesan       = $pesan;
        $balas->save();

        return Redirect::to("komplain/$id_komplain/balas");
    }

    public function accept($id)
    {
        $komplain = komplain::findOrFail($id);
        $komplain->status = '1';
        $komplain->save();

        return Redirect::to("komplain/index");
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $komplain = komplain::where('id_users','=',$user->id)->get();
        return view('komplain/detil', compact('komplain', 'user'));
    }
}
