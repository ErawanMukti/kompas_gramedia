<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\survei, App\User, App\ruangan;
use Auth, Validator, Input, Redirect;

class surveiController extends Controller
{
    private $title = "Data Survei";
    private $desc  = "Manajemen data survei";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        if ( Auth::user()->jenis == 'admin' ) {
            $survei = DB::table('surveis')
                 ->join('ruangans', 'surveis.id_ruangan', '=', 'ruangans.id')
                 ->select('surveis.*', 'ruangans.nama')
                 ->get();
        } else {
            $survei = DB::table('surveis')
                 ->join('ruangans', 'surveis.id_ruangan', '=', 'ruangans.id')
                 ->leftjoin('users', 'surveis.id_users', '=', 'users.id')
                 ->select('surveis.*', 'ruangans.nama', 'users.email')
                 ->where('surveis.id_users', '=', null)
                 ->orWhere('surveis.id_users', '=', Auth::user()->id)
                 ->get();
        }

        return view('survei/index', array(
            'title'  => $this->title,
            'desc'   => $this->desc,
            'survei' => $survei
        ));        
    }

    public function create()
    {
        $ruangan = ruangan::all();
        return view('survei.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'ruangan' => $ruangan
        ));
    }

    public function store(Request $request)
    {	
        $rules = array(
            'id_ruangan'     => 'required',
            'tanggal_survei' => 'required',
            'waktu_survei'   => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("survei/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$survei = survei::create($request->all());
        $survei->save();
        return redirect('survei/index')->with('alert-success', 'Data berhasil disimpan !!');
    }

    public function book($id)
    {
        $survei = survei::find($id);
        $survei->id_users = Auth::user()->id;
        $survei->save();
        return back();
    }

     public function destroy($id)
    {
        survei::destroy($id);
        return back()->with('alert_success','');
    }

}
