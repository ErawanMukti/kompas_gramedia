<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\area, App\gedung, App\ruangan, App\saran, App\User;
use Validator, Input, Redirect, Session;

class pageController extends Controller
{
    public function welcome()
    {
        $area = area::pluck('lokasi', 'id');
        $gedung = gedung::where('id_area', '=', 1)->pluck('nama_gedung', 'id');
        return view('welcome', compact('area', 'gedung'));
    }

    public function gedungAjax($id)
    {
        if($id == 0){
            $gedung = gedung::all();
        }else{
            $gedung = gedung::where('id_area','=',$id)->get();
        }
        return $gedung;
    }

    public function ruangan() 
    {
        $rules = array(
            'lokasi'      => 'required',
            'nama_gedung' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ( $validator->fails() ) {
            return Redirect::to('/')
                  ->withErrors($validator)
                  ->withInput(Input::all());
        } else {
            $area    = Input::get('lokasi');
            $gedung  = Input::get('nama_gedung');
            $ruangan = ruangan::where('id_gedung', $gedung)->get();

            $h_ruangan = array();
            foreach ($ruangan as $value) {
                array_push($h_ruangan, $this->html_ruangan($value));
            }

            return view('ruangan', array('ruangan' => $h_ruangan));
        }
    }

    private function html_ruangan($data) {        
        $foto  = asset("uploadfoto/$data->foto");
        $nama  = $data->nama;
        $jenis = $data->jenis_ruangan;
        $fasum = $data->fasilitas_umum;
        $fasin = $data->fasilitas_dalam_ruangan;
        $dpsit = $data->deposit;
        $schrg = $data->service_charge;

        $jml_saran = saran::where('id_ruangan', '=', $data->id)->count();

        $html = '
            <!-- Default box -->
            <div class="col-md-6">
                <!-- Box Comment -->
                <div class="box box-widget">
                    <div class="box-header with-border">
                      <div class="user-block">
                        <img class="img-circle" src="'. $foto .'" alt="User Image">
                        <span class="username"><a href="#">'. $nama .'</a></span>
                        <span class="description">Tipe Ruangan: '. $jenis .'</span>
                      </div>
                      <!-- /.user-block -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <img class="img-responsive pad" src="'. $foto .'" alt="Photo">

                      <p>Fasilitas Umum : '. $fasum .'</p>
                      <p>Fasilitas Dalam Ruangan : '. $fasin .'</p>
                      <p>Deposit : '. number_format($dpsit) .'</p>
                      <p>Service Charge : '. number_format($schrg) .'</p>
                      <span class="pull-right text-muted">'. $jml_saran .' saran</span>
                    </div>
                    <!-- /.box-body -->
                    '. $this->html_review($data->id) .'
                </div>
                <!-- /.box -->
            </div>
            <!-- /.box -->
        ';
        return $html;
    }

    public function html_review($id)
    {
        $html  = '';
        $saran = saran::where('id_ruangan', '=', $id)->get();
        if ( count($saran) > 0 ) {
            $html .= '<div class="box-footer box-comments">';
            foreach ($saran as $value) {
                $user = User::whereId($value->id_users)->get();

                $html .='
                    <div class="box-comment">
                        <!-- User image -->
                        <img class="img-circle img-sm" src="'. asset('assets/img/lte/boxed-bg.jpg') .'">

                        <div class="comment-text">
                            <span class="username">
                                '. $user[0]->nama .'
                                <span class="text-muted pull-right">'. date('d M Y', strtotime($value->tgl_saran)) .'</span>
                            </span><!-- /.username -->
                            '. $value->isi_saran .'
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->
                ';
            }
            $html .= '</div>';
        }
        return $html;
  }
}
