<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\User;
use App\ruangan;
use Validator, Auth, Input, Redirect, Session;

class userController extends Controller
{
    private $title = "Data User";
    private $desc  = "Manajemen data user";

    public function show()
    {
        $user = DB::table('users')
                 ->where('users.jenis', 'penyewa')
                 ->get();
                 
    	return view('user.index', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'user'  => $user
        ));        
    }

    public function create()
    {
        $user = user::all();
    	return view('user.add', array(
            'user'  => $user
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'nama'      => 'required|regex:/^[\pL\s\-]+$/u',
            'email'     => 'required|email',
            'password'  => 'required',
            'no_telp'   => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ( $validator->fails() ) {
            return Redirect::to('user/add')
                   ->withErrors($validator)
                   ->withInput(Input::all());
        } else {
        	$user = User::create($request->all());
        	$user->password=bcrypt($request->get('password'));
        	$user->save();
        	return redirect('/login');
        }
    }

    public function edit($id)
    {
        $user = user::findOrFail($id);
        return view('user.edit', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'user'  => $user
        ));
     }

    public function update(Request $request, $id)
    {
        $rules = array(
            'nama'    => 'required|regex:/^[\pL\s\-]+$/u',
            'no_telp' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ( $validator->fails() ) {
            return Redirect::to('user/$id/edit')
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $user = user::whereId($id)->first();

        $user->nama      = $request->get('nama');
        $user->institusi = $request->get('institusi');
        $user->no_telp   = $request->get('no_telp');
        if ( $request->get('password') != "" ) {
            $user->password=bcrypt($request->get('password'));
        }
        $user->save();
        return redirect('user/index');
    }

    public function destroy($id)
    {
        User::destroy($id);
        return back()->with('alert-success','');
    }

}
