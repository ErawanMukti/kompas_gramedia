<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\saran, App\ruangan;
use Auth, Validator, Input, Redirect;

class SaranController extends Controller
{
    private $title = "Data Ruangan";
    private $desc  = "Manajemen data ruangan";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
 	{
 		if ( Auth::user()->jenis == 'admin' ) {
        	$saran = DB::table('saran')
	             ->join('ruangans', 'saran.id_ruangan', '=', 'ruangans.id')
	             ->select('saran.*', 'ruangans.nama')
	             ->get();
 		} else {
	        $saran = DB::table('saran')
	             ->join('ruangans', 'saran.id_ruangan', '=', 'ruangans.id')
	             ->select('saran.*', 'ruangans.nama')
	             ->where('saran.id_users', '=', Auth::user()->id)
	             ->get();
 		}

		return view('saran.index', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'saran' => $saran
        ));        
	}

	public function create()
	{	
        $ruangan = DB::table('ruangans')
                 ->whereIn('id', function($qry) {
                    $qry->select('id_ruangan')
                        ->from('surveis')
                        ->where('id_users', '=', Auth::user()->id);
                 })
                 ->pluck('nama', 'id');

		return view('saran.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'ruangan' => $ruangan,
		));
	}

	public function store(Request $request)
	{
        $rules = array(
			'id_ruangan' => 'required', 
			'isi_saran'  => 'required', 
            'tgl_saran'  => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("saran/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

		$saran = saran::create($request->all());		
		$saran->save();
		return redirect('saran/index');
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
            ]);
	}

	public function update(Request $request, $id)
	{
		$saran = saran::update($request->all());		
		$saran->save();
		return back();
	}

	public function destroy($id)
	{
		saran::destroy($id);
		return back()->with('alert-success','');
	}

	public function detilpermintaan()
	{
		$user = Auth::user();
		$saran = saran::where('id_users','=',$user->id)->get();
		return view('saran/detil', compact('saran', 'user'));
	}
}
