<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\transaksi, App\permintaan, App\kontrak;
use Auth, Validator, Input, Redirect;

class transaksiController extends Controller
{
    private $title = "Data Transaksi";
    private $desc  = "Manajemen data transaksi";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        if ( Auth::user()->jenis == 'admin' ) {
            $transaksi = transaksi::all();
        } else {
            $transaksi = DB::table('transaksis')
                 ->join('kontraks', 'transaksis.id_kontrak', '=', 'kontraks.id')
                 ->select('transaksis.*')
                 ->where('kontraks.id_users', '=', Auth::user()->id)
                 ->get();
        }
    	return view('transaksi.index', array(
            'title'     => $this->title,
            'desc'      => $this->desc,
            'transaksi' => $transaksi
        ));        
    }

    public function penyewa()
    {
        $transaksi = transaksi::all();
        return view('transaksi.penyewa', compact('transaksi','user'));
    }

    public function create()
    {
        $kontrak = kontrak::pluck('id', 'id');

    	return view('transaksi.add', array(
            'title'     => $this->title,
            'desc'      => $this->desc,
            'kontrak'   => $kontrak
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'total_bayar'         => 'required|numeric',
            'tanggal_jatuh_tempo' => 'required',
            'tanggal_bayar'       => 'required',
            'id_kontrak'          => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("transaksi/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$transaksi = transaksi::create($request->all());
    	$transaksi->save();
    	return redirect('transaksi/index');
    }

     protected function validator(array $data)
    {
        return Validator::make($data, [
        ]);
    }

    public function edit($id)
    {
        $transaksi = transaksi::findOrFail($id);
        return view('transaksi.edit', compact('transaksi'));
    }

    public function update(Request $request, $id)
    {
        $transaksi = transaksi::update($request->all());
        $transaksi->save();
        return back();
    }

    public function destroy($id)
    {
        transaksi::destroy($id);
        return back()->with('alert-success','');
    }

    public function detilTransaksi()
    {
        $a = $kontrak->transaksi;
        $user = Auth::user();
        $transaksi = transaksi::where('id_kontrak','=', $a->id)->get();
        return view('transaksi/detil', compact('transaksi', 'user'));
    }

    public function cetak($id)
    {
        $transaksi = transaksi::findOrFail($id);
        return view('transaksi.cetak', compact('transaksi'));
    }
}
