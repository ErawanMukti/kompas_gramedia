<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\transaksi, App\kontrak;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Auth::user()->jenis == 'admin' ) {
            $transaksi = transaksi::all();
            $kontrak   = kontrak::all();
        } else {
            $transaksi = DB::table('transaksis')
                        ->join('kontraks', 'transaksis.id_kontrak', '=', 'kontraks.id')
                        ->select('transaksis.*')
                        ->where('kontraks.id_users', '=', Auth::user()->id)
                        ->get();

            $kontrak = kontrak::where('id_users', '=', Auth::user()->id)->get();
        }

        return view('home', array(
            'title'     => 'Dashboard',
            'desc'      => 'Aplikasi penyewaan ruangan Kompas Media',
            'transaksi' => $transaksi,
            'kontrak'   => $kontrak
        ));
    }
}
