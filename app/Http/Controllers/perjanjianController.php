<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\perjanjian;
use App\kontrak;
use Auth, Validator, Input, Redirect;

class perjanjianController extends Controller
{
    private $title = "Data Perjanjian";
    private $desc  = "Manajemen data perjanjian";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $perjanjian = perjanjian::all();
    	return view('perjanjian.index', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'perjanjian' => $perjanjian
        ));        
    }

    public function create()
    {
    	return view('perjanjian.add', array(
            'title' => $this->title,
            'desc'  => $this->desc
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'no_perjanjian'   => 'required',
            'nama_perjanjian' => 'required',
            'isi_pasal'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("perjanjian/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$perjanjian = perjanjian::create($request->all());
        $fileName = $request->file('isi_pasal')->getClientOriginalName();
        $request->file('isi_pasal')->move('isiPasal/', $fileName);
        $perjanjian->isi_pasal=$fileName;
    	$perjanjian->save();
    	return redirect('perjanjian/index');
    }

    public function edit($id)
    {
        $perjanjian = perjanjian::findOrFail($id);
        return view('perjanjian.edit', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'perjanjian' => $perjanjian
        ));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'no_perjanjian'   => 'required',
            'nama_perjanjian' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("perjanjian/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $perjanjian = perjanjian::whereId($id)->first();
        $perjanjian->no_perjanjian   = $request->get('no_perjanjian');
        $perjanjian->nama_perjanjian = $request->get('nama_perjanjian');
        if ( $request->file('isi_pasal') != null ) {
            $fileName = $request->file('isi_pasal')->getClientOriginalName();
            $request->file('isi_pasal')->move('isiPasal/', $fileName);
            $perjanjian->isi_pasal=$fileName;
        }

        $perjanjian->save();
        return redirect('perjanjian/index');
    }

    public function destroy($id)
    {
        perjanjian::destroy($id);
        return back()->with('alert-success','');
    }

     public function detilPerjanjian()
    {
        $a = $kontrak->ruangan;
        $user = Auth::user();
        $kontrak = kontrak::whereIdUsers($user->id)->get();
        $perjanjian = perjanjian::where('id_lihat','=', $user->id)->get();
        //$perjanjian = DB::table('perjanjians')->select()
        return view('perjanjian/detil', compact('kontrak', 'perjanjian', 'user'));
    }

}

