<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\permintaan, App\User, App\transaksi, App\ruangan, App\kontrak;
use Auth, Validator, Input, Redirect;


class permintaanController extends Controller
{
    private $title = "Data Permintaan";
    private $desc  = "Manajemen data permintaan";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
	{
		$block = false;
 		if ( Auth::user()->jenis == 'admin' ) {
	        $permintaan = DB::table('permintaan')
	             ->join('kontraks', 'permintaan.id_kontrak', '=', 'kontraks.id')
	             ->select('permintaan.*', 'kontraks.no_kontrak')
	             ->get();
		} else {
			$jml_kontrak = kontrak::where('id_users', '=', Auth::user()->id)->count();

	        $transaksi = DB::table('transaksis')
                     	->whereMonth('tanggal_bayar', '=', date('m'))
                     	->whereYear('tanggal_bayar', '=', date('Y'))
	                 	->whereIn('id_kontrak', function($qry) {
	                    	$qry->select('id')
	                        	->from('kontraks')
	                        	->where('id_users', '=', Auth::user()->id);
	                 	})
	                 	->whereIn('jenis_bayar', array('Biaya Overtime AC', 'Pembayaran PLN', 'Biaya Cleaning Service'))
	                 	->get();
	        $jml_transaksi = count($transaksi);

	        //cek jumlah transaksi. dalam 1 bulan harus ada 3 transaksiL AC, PLN dan
	        //Cleaning Service. Jika punya n kontrak maka (n x 3) transaksi
	        if ( $jml_transaksi < ($jml_kontrak * 3) ) {
				$block = true;
	        }
	        $permintaan = DB::table('permintaan')
	             ->join('kontraks', 'permintaan.id_kontrak', '=', 'kontraks.id')
	             ->select('permintaan.*', 'kontraks.no_kontrak')
	             ->where('permintaan.id_users', '=', Auth::user()->id)
	             ->get();
		}

		return view('permintaan.index', array(
            'title'   	 => $this->title,
            'desc'    	 => $this->desc,
            'permintaan' => $permintaan,
            'block'		 => $block
        ));        
	}


	public function create()
	{
		$user    = User::find(Auth::user()->id);
        $ruangan = DB::table('ruangans')
                 ->whereIn('id', function($qry) {
                    $qry->select('id_ruangan')
                        ->from('kontraks')
                        ->where('id_users', '=', Auth::user()->id);
                 })
                 ->get();
		$kontrak = kontrak::where('id_users', '=', Auth::user()->id)->get();
		
		return view('permintaan.add', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
			'user' 	  => $user,
			'ruangan' => $ruangan,
			'kontrak' => $kontrak
		));
	}

	public function store(Request $request)
	{
        $rules = array(
			'nama_permintaan' => 'required', 
            'isi_permintaan' => 'required',
            'tanggal_pengajuan' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("permintaan/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

		$permintaan = permintaan::create($request->all());
		$permintaan->save();

		return redirect('permintaan/index');
	}

	public function edit($id)
	{
		$permintaan = permintaan::findOrFail($id);
		return view('permintaan.edit', compact('permintaan','user','transaksi','ruangan'));
	}

	public function update(Request $request, $id)
	{
		$permintaan = permintaan::update($request->all());
		$fileName = $request->file('isi_permintaan')->getClientOriginalName();
        $request->file('isi_permintaan')->move('isipermintaan/', $fileName);
        $permintaan->isi_permintaan=$fileName;
		$permintaan->save();
		return back();
	}

	public function destroy($id)
	{
		permintaan::destroy($id);
		return back()->with('alert-success','');
	}

	public function accept($id)
	{
		$permintaan = permintaan::findOrFail($id);
		$permintaan->status = 1;
		$permintaan->save();

		return redirect('permintaan/index');
	}

	public function denied($id)
	{
		$permintaan = permintaan::findOrFail($id);
		$permintaan->status = 2;
		$permintaan->save();

		return redirect('permintaan/index');
	}

	public function laporan()
	{
		return view('permintaan.laporan', array(
            'title'   => $this->title,
            'desc'    => $this->desc
		));
	}

	public function cetak()
	{
		$jenis = Input::get('nama_permintaan');
		$tgl   = Input::get('tanggal_pengajuan');

        $permintaan = DB::table('permintaan')
             ->join('kontraks', 'permintaan.id_kontrak', '=', 'kontraks.id')
             ->join('users', 'permintaan.id_users', '=', 'users.id')
             ->join('ruangans', 'kontraks.id_ruangan', '=', 'ruangans.id')
             ->select('permintaan.*', 'kontraks.no_kontrak', 'users.email', 'ruangans.nama')
             ->where('permintaan.nama_permintaan', '=', $jenis)
             ->whereDate('permintaan.tanggal_pengajuan', '=', date('Y-m-d', strtotime($tgl)))
             ->get();

		return view('permintaan.cetak', array(
            'title'      => $this->title,
            'desc'       => $this->desc,
            'permintaan' => $permintaan,
            'jenis'		 => $jenis,
            'tgl'		 => $tgl
		));
	}

    public function nota($id)
    {
        $permintaan = permintaan::findOrFail($id);
        $kontrak    = kontrak::find($permintaan->id_kontrak);
        $ruangan    = ruangan::find($kontrak->id_ruangan);

        return view('permintaan.nota',array(
        	'permintaan' => $permintaan, 
        	'kontrak' 	 => $kontrak,
        	'ruangan' 	 => $ruangan
        ));
    }
}