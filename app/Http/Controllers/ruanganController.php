<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\ruangan, App\gedung, App\user, App\kontrak, App\area;
use Auth, Validator, Input, Redirect;

class ruanganController extends Controller
{
    private $title = "Data Ruangan";
    private $desc  = "Manajemen data ruangan";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $ruangan = ruangan::all();
        return view('ruangan.index', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'ruangan' => $ruangan
        ));        
    }

    public function publicpenyewa()
    {
        $ruangan = ruangan::all();
        return view('ruangan.publicpenyewa', compact('ruangan', 'user'));
    }

    public function create()
    {
        $user = User::all();
        $gedung = gedung::all();
    	return view('ruangan.add', array(
            'title'  => $this->title,
            'desc'   => $this->desc,
            'gedung' => $gedung
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'nama'                    => 'required',
            'fasilitas_umum'          => 'required',
            'fasilitas_dalam_ruangan' => 'required',
            'jenis_ruangan'           => 'required',
            'deposit'                 => 'required|numeric',
            'harga_perm'              => 'required|numeric',
            'service_charge'          => 'required|numeric',
            'foto'                    => 'required',
            'luas_ruangan'            => 'required|numeric',
            'pajak_pph'               => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("ruangan/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$ruangan  = ruangan::create($request->all());
        $fileName = $request->file('foto')->getClientOriginalName();
        $request->file('foto')->move('uploadfoto/',$fileName);
        $ruangan->foto=$fileName;
    	$ruangan->save();
    	return redirect('ruangan/index');
    }

    public function detil(Request $request, $id)
    {
        $ruangan = DB::table('ruangans')
             ->join('gedungs', 'ruangans.id_gedung', '=', 'gedungs.id')
             ->join('areas', 'gedungs.id_area', '=', 'areas.id')
             ->select('ruangans.*', 'gedungs.nama_gedung', 'areas.lokasi')
             ->where('ruangans.id', '=', $id)
             ->get();
        $ruangan_class = ruangan::find($id);

        return view('ruangan.detil', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'ruangan' => $ruangan[0],
            '_ruangan'=> $ruangan_class
        ));
    }

    public function edit($id)
    {
        $ruangan = ruangan::findOrFail($id);
        return view('ruangan.edit', array(
            'title'   => $this->title,
            'desc'    => $this->desc,
            'ruangan' => $ruangan
        ));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'nama'                    => 'required',
            'fasilitas_umum'          => 'required',
            'fasilitas_dalam_ruangan' => 'required',
            'jenis_ruangan'           => 'required',
            'deposit'                 => 'required|numeric',
            'harga_perm'              => 'required|numeric',
            'service_charge'          => 'required|numeric',
            'luas_ruangan'            => 'required|numeric',
            'pajak_pph'               => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("ruangan/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $ruangan = ruangan::whereId($id)->first();
        $ruangan->nama = $request->get('nama');
        $ruangan->fasilitas_umum = $request->get('fasilitas_umum');
        $ruangan->fasilitas_dalam_ruangan = $request->get('fasilitas_dalam_ruangan');
        $ruangan->jenis_ruangan = $request->get('jenis_ruangan');
        $ruangan->deposit = $request->get('deposit');
        $ruangan->harga_perm = $request->get('harga_perm');
        $ruangan->service_charge = $request->get('service_charge');

        if ( $request->file('foto') != null ) {
            $fileName = $request->file('foto')->getClientOriginalName();
            $request->file('foto')->move('uploadfoto/',$fileName);
            $ruangan->foto=$fileName;
        }

        $ruangan->luas_ruangan = $request->get('luas_ruangan');
        $ruangan->biaya_overtime_ac = $request->get('biaya_overtime_ac');
        $ruangan->pajak_pph = $request->get('pajak_pph');
        $ruangan->save();

        return redirect('ruangan/index');
    }

    public function destroy($id)
    {
        ruangan::destroy($id);
        return back()->with('alert_success','');
    }

}
