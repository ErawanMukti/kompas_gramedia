<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\area;
use Validator, Input, Redirect;

class areaController extends Controller
{
    private $title = "Data Gedung";
    private $desc  = "Manajemen data gedung";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $area = area::all();
        return view('area.index', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'area'  => $area
        ));        
    }

    public function create()
    {
    	return view('area.add', array(
            'title' => $this->title,
            'desc'  => $this->desc
        ));
    }

    public function store(Request $request)
    {
        $rules = array('lokasi' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("area/create")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

    	$area = area::create($request->all());
    	$area->save();
    	return redirect('area/index');
    }

    public function edit($id)
    {
        $area = area::findOrFail($id);
        return view('area/edit', array(
            'title' => $this->title,
            'desc'  => $this->desc,
            'area'  => $area
        ));
    }

    public function update($id)
    {
        $rules = array('lokasi' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ( $validator->fails() ) {
            return Redirect::to("area/$id/edit")
                   ->withErrors($validator)
                   ->withInput(Input::all());
        }

        $area = area::find($id);
        $area->lokasi = Input::get('lokasi');
        $area->save();

        return redirect('area/index');
    }

    public function destroy($id)
    {
        area::destroy($id);
        return back()->with('alert-success','');
    }

}
