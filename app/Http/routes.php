<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
    //return view('welcome');
//});

Route::get('/', 'pageController@welcome');
Route::post('/listruangan', 'pageController@ruangan');

Route::get('user/add', 'userController@create');
Route::post('user/store', 'userController@store');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/registerAdmin', 'registerAdmin@create');
Route::post('registerAdmin/store', 'registerAdmin@store');

Route::get('registerAdmin', 'registerPenyewa@create');
Route::post('registerPenyewa', 'registerPenyewa@store');

Route::resource('user', 'userController');

Route::resource('area', 'areaController');

Route::resource('gedung', 'gedungController');

Route::resource('ruangan', 'ruanganController');
Route::get('/ruangan/{id}/detail/', 'ruanganController@detil');

Route::resource('survei', 'surveiController');
Route::get('/survei/{id}/book/', 'surveiController@book');

Route::resource('saran', 'SaranController');

Route::resource('perjanjian', 'perjanjianController');

Route::resource('kontrak', 'kontrakController');
Route::get('/kontrak/{id}/detail/', 'kontrakController@detil');

Route::resource('permintaan', 'permintaanController');
Route::get('/permintaan/{id}/accept/', 'permintaanController@accept');
Route::get('/permintaan/{id}/denied/', 'permintaanController@denied');
Route::get('/permintaan/{id}/nota/', 'permintaanController@nota');

Route::get('/laporan/permintaan/', 'permintaanController@laporan');
Route::get('/laporan/permintaan_cetak/', 'permintaanController@cetak');

Route::resource('komplain', 'komplainController');
Route::get('/komplain/{id}/balas', 'komplainController@balas');
Route::get('/komplain/{id}/accept', 'komplainController@accept');
Route::post('/komplain/balas', 'komplainController@simpanbalas');

Route::resource('section', 'sectionController');

Route::resource('kuisioner', 'kuisionerController');

Route::get('kuesioner/isi', 'kuisionerController@isi');
Route::post('kuesioner/isi', 'kuisionerController@simpan_isi');
Route::get('kuesioner/hasil', 'kuisionerController@hasil');
Route::post('kuesioner/grafik', 'kuisionerController@grafik');

Route::resource('transaksi','transaksiController');
Route::get('/transaksi/{id}/cetak/', 'transaksiController@cetak');

Route::get('/jsarea/{id}','pageController@gedungAjax');
Route::get('/jsgedung/{id}', 'pageController@ruanganAjax');