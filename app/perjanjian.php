<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class perjanjian extends Model
{
    protected $table = 'perjanjians';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'no_perjanjian', 'nama_perjanjian', 'isi_pasal'];

    public function kontrak()
    {
    	return $this->hasMany('App\kontrak');
    }
}
