<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class isi extends Model
{
    protected $table = 'isis';
   	protected $primaryKey = 'id';
   	protected $fillable = ['id', 'jawaban', 'id_kontrak', 'id_kuisioner_kepuasan'];

   	public function kontrak()
   	{
   		return $this->BelongsToMany('App\kontrak');
   	}

   	public function kuisionerKepuasan()
   	{
   		return $this->BelongsToMany('App\kuisionerKepuasan');
   	}
}
