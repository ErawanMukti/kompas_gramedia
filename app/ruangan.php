<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruangan extends Model
{
    protected $table ='ruangans';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'nama', 'fasilitas_umum', 'fasilitas_dalam_ruangan', 'jenis_ruangan', 'deposit', 'harga_perm', 'service_charge', 'foto', 'luas_ruangan', 'biaya_overtime_ac', 'pajak_pph', 'status','id_gedung','id_lihat'];

    public function kontrak()
    {
    	return $this->hasMany('App\kontrak', 'id_ruangan', 'id');
    }

    public function gedung()
    {
    	return $this->BelongsTo('App\gedung');
    }

    public function manajemen()
    {
    	return $this->hasMany('App\manajemen');
    }
}
