<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class manajemen extends Model
{
    protected $table = 'manajemens';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'komentar', 'id_users', 'id_ruangan'];

    public function users()
    {
    	 return $this->BelongsToMany('App\users');
    }

    public function ruangan()
    {
    	return $this->BelongsToMany('App\ruangan');
    }
}
