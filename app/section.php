<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class section extends Model
{
    
    protected $table = 'sections';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'kategori'];

    public function kuisionerKepuasan()
    {
    	return $this->hasMany('App\kuisionerKepuasan', 'id_section','id');
    }
}
