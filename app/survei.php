<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class survei extends Model
{
    protected $table = 'surveis';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'id_users', 'id_ruangan', 'tanggal_survei', 'waktu_survei'];

    public function users()
    {
        return $this->hasMany('App\User', 'id', 'id_users');
    }

    public function lihat()
    {
    	return $this->hasMany('App\lihat');
    }
}
