<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kuisionerKepuasan extends Model
{
    protected $table = 'kuisioner_kepuasans';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'pertanyaan', 'id_section'];

    public function isi()
    {
    	return $this->hasMany('App\isi');
    }

    public function section()
    {
    	return $this->BelongsTo('App\section', "id");
    }


}
