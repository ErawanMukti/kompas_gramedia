<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gedung extends Model
{
    protected $table ='gedungs';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'nama_gedung', 'alamat_gedung', 'no_telp_gedung', 'id_area'];

    public function area()
    {
    	return $this->BelongsTo('App\area');
    }

    public function ruangan()
    {
    	return $this->hasMany('App\ruangan');
    }
}
