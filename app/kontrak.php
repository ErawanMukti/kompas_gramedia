<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kontrak extends Model
{
    protected $table = 'kontraks';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'no_kontrak', 'jangka_waktu', 'tanggal_masuk', 'tanggal_keluar', 'file_kontrak', 'id_users', 'id_ruangan', 'id_perjanjian', 'id_komplain'];

    public function kuisionerkepuasan()
    {
    	return $this->hasMany('App\kuisionerkepuasan');
    }

    public function komplain()
    {
    	return $this->hasMany('App\komplain');
    }

    public function users()
    {
    	return $this->BelongsTo('App\users');
    }

    public function ruangan()
    {	
    	return $this->BelongsTo('App\ruangan', 'id');
    }

    public function transaksi()
    {
    	return $this->hasMany('App\transaksi');
    }

    public function perjanjian()
    {
    	return $this->BelongsTo('App\perjanjian', 'id_perjanjian');
    }
}
