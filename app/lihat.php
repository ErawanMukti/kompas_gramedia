<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lihat extends Model
{
    protected $table = 'lihats';
    protected $primaryKey = 'id'; 
    protected $fillable = ['id', 'komentar', 'id_survei', 'id_ruangan'];

    public function survei()
    {
    	return $this->BelongsToMany('App\survei');
    }

    public function ruangan()
    {
    	return $this->BelongsToMany('App\ruangan');
    }
}
