<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class area extends Model
{
    protected $table = "areas";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'lokasi'];

    public function gedung()
    {
    	return $this->hasMany('App\gedung');
    }

}
