<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class balas extends Model
{
    protected $table = "komplain_balas";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'id_komplain', 'id_users', 'tgl_balas', 'pesan'];

}
