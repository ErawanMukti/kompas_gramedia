<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permintaan extends Model
{
	protected $table = 'permintaan';
	protected $primaryKey = 'id';
	protected $fillable = ['id','nama_permintaan','isi_permintaan','tanggal_pengajuan','id_users', 'id_kontrak', 'id_transaksi'];

	public function users()
    {
    	return $this->BelongsTo('App\users');
    }

    public function ruangan()
    {	
    	return $this->BelongsTo('App\ruangan', 'id', 'nama');
    }

    public function transaksi()
    {
    	return $this->BelongsTo('App\transaksi');
    }
}