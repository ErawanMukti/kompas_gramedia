<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komplain extends Model
{
    protected $table = 'komplains';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'subjek', 'isi_komplain', 'tanggal_komplain', 'id_users', 'id_kontrak'];

    /*public function komplain()
    {
    	return $this->hasMany('App\komplain');
    }*/
    public function kontrak()
    {
    	return $this->BelongsTo('App\kontrak');
    }
    public function users()
    {
    	return $this->BelongsTo('App\users');
    }
}
