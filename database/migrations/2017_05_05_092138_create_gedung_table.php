<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGedungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gedungs', function (Blueprint $table){
            $table->increments('id');
            $table->string('nama_gedung');
            $table->string('alamat_gedung');
            $table->string('no_telp_gedung');
            $table->integer('id_area')->unsigned();
            $table->foreign('id_area')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gedungs');
    }
}
