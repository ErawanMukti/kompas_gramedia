<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomplainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komplains', function (Blueprint $table){
            $table->increments('id');
            $table->string('isi_komplain');
            $table->datetime('tanggal_komplain');
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users');
            $table->integer('id_kontrak')->unsigned();
            $table->foreign('id_kontrak')->references('id')->on('kontraks');
            $table->integer('id_komplain')->unsigned();
            $table->foreign('id_komplain')->references('id')->on('komplains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('komplains');
    }
}
