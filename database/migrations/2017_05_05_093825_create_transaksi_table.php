<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('transaksis', function (Blueprint $table){
            $table->increments('id');
            $table->double('total_bayar');
            $table->datetime('tanggal_jatuh_tempo');
            $table->datetime('tanggal_bayar');
            $table->integer('id_kontrak')->unsigned();
            $table->foreign('id_kontrak')->references('id')->on('kontraks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaksis');
    }
}
