<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ruangans', function (Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->string('fasilitas_umum');
            $table->string('fasilitas_dalam_ruangan');
            $table->string('jenis_ruangan');
            $table->double('deposit');
            $table->double('harga_perm');
            $table->double('service_charge');
            $table->string('foto');
            $table->double('luas_ruangan');
            $table->double('biaya_overtime_ac')->nullable();
            $table->double('pajak_pph');
            $table->integer('id_gedung')->unsigned();
            $table->foreign('id_gedung')->references('id')->on('gedungs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ruangans');
    }
}
