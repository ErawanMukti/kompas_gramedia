<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontrakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontraks', function (Blueprint $table){
            $table->increments('id');
            $table->integer('jangka_waktu');
            $table->datetime('tanggal_masuk');
            $table->datetime('tanggal_keluar');
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users');
            $table->integer('id_ruangan')->unsigned();
            $table->foreign('id_ruangan')->references('id')->on('ruangans');
            $table->integer('id_perjanjian')->unsigned();
            $table->foreign('id_perjanjian')->references('id')->on('perjanjians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('kontraks');
    }
}
