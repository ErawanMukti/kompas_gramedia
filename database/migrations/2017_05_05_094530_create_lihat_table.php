<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLihatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lihats', function (Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('komentar')->nullable();
            $table->integer('id_survei')->unsigned();
            $table->foreign('id_survei')->references('id')->on('surveis')->onDelete('cascade');
            $table->integer('id_ruangan')->unsigned();
            $table->foreign('id_ruangan')->references('id')->on('ruangans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lihats');
    }
}
