<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isis', function (Blueprint $table){
            $table->increments('id');
            $table->string('jawaban');
            $table->integer('id_kontrak')->unsigned();
            $table->foreign('id_kontrak')->references('id')->on('kontraks')->onDelete('cascade');
            $table->integer('id_kuisioner_kepuasan')->unsigned();
            $table->foreign('id_kuisioner_kepuasan')->references('id')->on('kuisioner_kepuasans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('isis');
    }
}
