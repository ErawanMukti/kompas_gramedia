<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('linetelepon')->nullable();
            $table->string('dayalistrik')->nullable();
            $table->string('cleaning')->nullable();
            $table->string('reklame')->nullable();
            $table->string('kegiatan')->nullable();
            $table->string('renovasi')->nullable();
            $table->string('sewa')->nullable();
            $table->string('lain')->nullable();
            $table->integer('id_transaksi')->unsigned();
            $table->foreign('id_transaksi')->references('id')->on('transaksis');
            $table->integer('id_ruangan')->unsigned();
            $table->foreign('id_ruangan')->references('id')->on('ruangans');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permintaan');
    }
}
