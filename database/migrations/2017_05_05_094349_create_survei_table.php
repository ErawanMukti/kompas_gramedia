<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveis', function (Blueprint $table){
            $table->increments('id');
            $table->string('nama_surveyor');
            $table->string('alamat_surveyor');
            $table->string('no_telp_surveyor');
            $table->string('email_surveyor');
            $table->date('tanggal_survei');
            $table->time('waktu_survei');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveis');
    }
}
