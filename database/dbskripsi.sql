-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Sep 2017 pada 06.33
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbskripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
`id` int(10) unsigned NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `areas`
--

INSERT INTO `areas` (`id`, `lokasi`, `created_at`, `updated_at`) VALUES
(1, 'Surabaya', '2017-08-03 23:55:38', '2017-08-03 23:55:38'),
(2, 'Jakarta', '2017-08-03 23:55:49', '2017-08-03 23:55:49'),
(3, 'Yogyakarta', '2017-08-03 23:56:10', '2017-08-03 23:56:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gedungs`
--

CREATE TABLE IF NOT EXISTS `gedungs` (
`id` int(10) unsigned NOT NULL,
  `nama_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp_gedung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_area` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `gedungs`
--

INSERT INTO `gedungs` (`id`, `nama_gedung`, `alamat_gedung`, `no_telp_gedung`, `id_area`, `created_at`, `updated_at`) VALUES
(1, 'KG Jemursari', 'Jl Raya Jemursari 1', '03112341234', 1, '2017-08-04 07:01:20', '2017-08-04 07:01:20'),
(2, 'KG Palmerah', 'Jl Palmerah Barat 1 - 12', '02112341231', 2, '2017-08-04 14:02:00', '2017-08-04 07:02:00'),
(3, 'KG Yos Surdarso', 'Jl Yos Sudarso 21', '0234123432100000000000000', 3, '2017-08-31 14:15:23', '2017-08-31 07:15:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `isis`
--

CREATE TABLE IF NOT EXISTS `isis` (
`id` int(10) unsigned NOT NULL,
  `jawaban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_kontrak` int(10) unsigned NOT NULL,
  `id_kuisioner_kepuasan` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `komplains`
--

CREATE TABLE IF NOT EXISTS `komplains` (
`id` int(10) unsigned NOT NULL,
  `isi_komplain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_komplain` datetime NOT NULL,
  `id_users` int(10) unsigned NOT NULL,
  `id_kontrak` int(10) unsigned NOT NULL,
  `id_komplain` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontraks`
--

CREATE TABLE IF NOT EXISTS `kontraks` (
`id` int(10) unsigned NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `tanggal_masuk` datetime NOT NULL,
  `tanggal_keluar` datetime NOT NULL,
  `file_kontrak` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_users` int(10) unsigned NOT NULL,
  `id_ruangan` int(10) unsigned NOT NULL,
  `id_perjanjian` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kontraks`
--

INSERT INTO `kontraks` (`id`, `jangka_waktu`, `tanggal_masuk`, `tanggal_keluar`, `file_kontrak`, `id_users`, `id_ruangan`, `id_perjanjian`, `created_at`, `updated_at`) VALUES
(2, 5, '2017-08-05 00:00:00', '2022-05-04 00:00:00', 'copi.docx', 3, 1, 1, '2017-08-13 17:40:18', '2017-08-13 10:40:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuisioner_kepuasans`
--

CREATE TABLE IF NOT EXISTS `kuisioner_kepuasans` (
`id` int(10) unsigned NOT NULL,
  `pertanyaan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_kuisioner` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lihats`
--

CREATE TABLE IF NOT EXISTS `lihats` (
`id` int(10) unsigned NOT NULL,
  `komentar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_survei` int(10) unsigned NOT NULL,
  `id_ruangan` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemens`
--

CREATE TABLE IF NOT EXISTS `manajemens` (
`id` int(10) unsigned NOT NULL,
  `komentar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_users` int(10) unsigned NOT NULL,
  `id_ruangan` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_05_05_092130_create_area_table', 1),
('2017_05_05_092138_create_gedung_table', 1),
('2017_05_05_092144_create_ruangan_table', 1),
('2017_05_05_092458_create_perjanjian_table', 1),
('2017_05_05_092673_create_kontrak_table', 1),
('2017_05_05_093825_create_transaksi_table', 1),
('2017_05_05_094138_create_komplain_table', 1),
('2017_05_05_094139_create_section_table', 1),
('2017_05_05_094200_create_kuisioner_kepuasan_table', 1),
('2017_05_05_094349_create_survei_table', 1),
('2017_05_05_094423_create_manajemen_table', 1),
('2017_05_05_094530_create_lihat_table', 1),
('2017_05_05_094545_create_isi_table', 1),
('2017_09_17_163419_buat_tabel_laporanpermintaan', 2),
('2017_09_17_163419_create_permintaan_table', 3),
('2017_09_19_161719_create_permintaan_table', 4),
('2017_09_19_162610_create_permintaan_table', 5),
('2017_09_19_181711_create_permintaan_table', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perjanjians`
--

CREATE TABLE IF NOT EXISTS `perjanjians` (
`id` int(10) unsigned NOT NULL,
  `nama_perjanjian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi_pasal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `perjanjians`
--

INSERT INTO `perjanjians` (`id`, `nama_perjanjian`, `isi_pasal`, `created_at`, `updated_at`) VALUES
(1, 'Perjanjian 1', 'tambahan.txt', '2017-08-13 04:39:55', '2017-08-12 21:39:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permintaan`
--

CREATE TABLE IF NOT EXISTS `permintaan` (
`id` int(10) unsigned NOT NULL,
  `linetelepon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dayalistrik` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cleaning` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reklame` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kegiatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `renovasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sewa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_transaksi` int(10) unsigned NOT NULL,
  `id_ruangan` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangans`
--

CREATE TABLE IF NOT EXISTS `ruangans` (
`id` int(10) unsigned NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fasilitas_umum` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fasilitas_dalam_ruangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_ruangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deposit` double NOT NULL,
  `harga_perm` double NOT NULL,
  `service_charge` double NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `luas_ruangan` double NOT NULL,
  `biaya_overtime_ac` double DEFAULT NULL,
  `pajak_pph` double NOT NULL,
  `id_gedung` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `ruangans`
--

INSERT INTO `ruangans` (`id`, `nama`, `fasilitas_umum`, `fasilitas_dalam_ruangan`, `jenis_ruangan`, `deposit`, `harga_perm`, `service_charge`, `foto`, `luas_ruangan`, `biaya_overtime_ac`, `pajak_pph`, `id_gedung`, `created_at`, `updated_at`) VALUES
(1, 'Jemursari Timur 1', 'Parkiran', 'Pantry', 'standar', 100000, 100000, 10000, '9.PNG', 12, 100000, 0, 1, '2017-08-13 04:25:19', '2017-08-12 21:25:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
`id` int(10) unsigned NOT NULL,
  `kategori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `sections`
--

INSERT INTO `sections` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(4, 'Kebersihan Ruangan', '2017-08-14 05:30:11', '2017-08-13 22:30:11'),
(5, 'Keamanan', '2017-08-14 05:31:00', '2017-08-13 22:31:00'),
(6, 'Kebersihan Taman', '2017-08-14 05:31:39', '2017-08-13 22:31:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surveis`
--

CREATE TABLE IF NOT EXISTS `surveis` (
`id` int(10) unsigned NOT NULL,
  `nama_surveyor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_surveyor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp_surveyor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_surveyor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_survei` date NOT NULL,
  `waktu_survei` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_users` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksis`
--

CREATE TABLE IF NOT EXISTS `transaksis` (
`id` int(10) unsigned NOT NULL,
  `total_bayar` double NOT NULL,
  `tanggal_jatuh_tempo` datetime NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `id_kontrak` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `institusi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `institusi`, `no_telp`, `jenis`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Derry Ferdians', 'derryfer@gmail.com', '$2y$10$XoO9DLFgcsE69sAMgUqGzuqh0QUOqJ3oCzDeaEh0Y/W6pbwfqg58C', 'PT Besi', '08112341234', 'surveyor', 'U6wvKJTuLk7Be9hPCJv3WtuyKBwllelSBfNR72nP0mRY4tKX7NF8cDPqe5uT', '2017-08-01 09:50:22', '2017-09-19 11:56:50'),
(5, 'Yovita Rosalin', 'rosalinyovita@gmail.com', '$2y$10$.4kyaR.g8R4ULFM9Qc9uReNf9gg4a7yuAA3X3bCvlLmpMcTILYusa', 'KG Surabaya Jemur', '03112341234', 'admin', 'MVF8xJfzv1vE1dimyJrSk5x2GiSvp2gwtBJaZeTFHR8qidyDvFhJ3TJQczzs', '2017-08-01 17:54:46', '2017-09-19 11:56:36'),
(6, 'Yovita', 'yovitarosalin@ymail.com', '$2y$10$YSJDImgx/mXwH4cx2gbuQ.DmxL6Zl7A3AmnvWQUQy.ptIawk5UrlS', 'KG Jakarta', '02112341234', 'pimpinan', 'eYnItsHZk1U5dl0i196ovB1fVtYFjgj7JJeKdISFVveB6ouAtMzsv2Jzb1Hs', '2017-08-01 18:09:14', '2017-09-19 10:28:20'),
(10, 'Daniel', 'danski@gmail.com', '$2y$10$JyyFVyOg7AsjUB8VGSh3Desc.ft1m8keJALicPxrd2X14hoPPxMNe', 'PT Maju', '03112341234', 'penyewa', 'asgNVBqvcsous4jGtx5Tk7QVoolJdDw2JPjkElXqmyGZ1OpOq83oWr4qOsQd', '2017-08-02 22:02:15', '2017-09-19 21:32:38'),
(11, '', '', '$2y$10$rqzFVRRTvPG8lxfbPNHF/OWcKM/GzNDpJfJ4MFS7Ufor5.iELNKYi', '', '', 'surveyor', NULL, '2017-08-13 22:39:27', '2017-08-13 22:39:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gedungs`
--
ALTER TABLE `gedungs`
 ADD PRIMARY KEY (`id`), ADD KEY `gedungs_id_area_foreign` (`id_area`);

--
-- Indexes for table `isis`
--
ALTER TABLE `isis`
 ADD PRIMARY KEY (`id`), ADD KEY `isis_id_kontrak_foreign` (`id_kontrak`), ADD KEY `isis_id_kuisioner_kepuasan_foreign` (`id_kuisioner_kepuasan`);

--
-- Indexes for table `komplains`
--
ALTER TABLE `komplains`
 ADD PRIMARY KEY (`id`), ADD KEY `komplains_id_users_foreign` (`id_users`), ADD KEY `komplains_id_kontrak_foreign` (`id_kontrak`), ADD KEY `komplains_id_komplain_foreign` (`id_komplain`);

--
-- Indexes for table `kontraks`
--
ALTER TABLE `kontraks`
 ADD PRIMARY KEY (`id`), ADD KEY `kontraks_id_users_foreign` (`id_users`), ADD KEY `kontraks_id_ruangan_foreign` (`id_ruangan`), ADD KEY `kontraks_id_perjanjian_foreign` (`id_perjanjian`);

--
-- Indexes for table `kuisioner_kepuasans`
--
ALTER TABLE `kuisioner_kepuasans`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lihats`
--
ALTER TABLE `lihats`
 ADD PRIMARY KEY (`id`), ADD KEY `lihats_id_survei_foreign` (`id_survei`), ADD KEY `lihats_id_ruangan_foreign` (`id_ruangan`);

--
-- Indexes for table `manajemens`
--
ALTER TABLE `manajemens`
 ADD PRIMARY KEY (`id`), ADD KEY `manajemens_id_users_foreign` (`id_users`), ADD KEY `manajemens_id_ruangan_foreign` (`id_ruangan`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `perjanjians`
--
ALTER TABLE `perjanjians`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
 ADD PRIMARY KEY (`id`), ADD KEY `permintaan_id_transaksi_foreign` (`id_transaksi`), ADD KEY `permintaan_id_ruangan_foreign` (`id_ruangan`);

--
-- Indexes for table `ruangans`
--
ALTER TABLE `ruangans`
 ADD PRIMARY KEY (`id`), ADD KEY `ruangans_id_gedung_foreign` (`id_gedung`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surveis`
--
ALTER TABLE `surveis`
 ADD PRIMARY KEY (`id`), ADD KEY `surveis_id_users_foreign` (`id_users`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
 ADD PRIMARY KEY (`id`), ADD KEY `transaksis_id_kontrak_foreign` (`id_kontrak`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gedungs`
--
ALTER TABLE `gedungs`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `isis`
--
ALTER TABLE `isis`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `komplains`
--
ALTER TABLE `komplains`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontraks`
--
ALTER TABLE `kontraks`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kuisioner_kepuasans`
--
ALTER TABLE `kuisioner_kepuasans`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lihats`
--
ALTER TABLE `lihats`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manajemens`
--
ALTER TABLE `manajemens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perjanjians`
--
ALTER TABLE `perjanjians`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ruangans`
--
ALTER TABLE `ruangans`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `surveis`
--
ALTER TABLE `surveis`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `gedungs`
--
ALTER TABLE `gedungs`
ADD CONSTRAINT `gedungs_id_area_foreign` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id`);

--
-- Ketidakleluasaan untuk tabel `isis`
--
ALTER TABLE `isis`
ADD CONSTRAINT `isis_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `isis_id_kuisioner_kepuasan_foreign` FOREIGN KEY (`id_kuisioner_kepuasan`) REFERENCES `kuisioner_kepuasans` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `komplains`
--
ALTER TABLE `komplains`
ADD CONSTRAINT `komplains_id_komplain_foreign` FOREIGN KEY (`id_komplain`) REFERENCES `komplains` (`id`),
ADD CONSTRAINT `komplains_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`),
ADD CONSTRAINT `komplains_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `kontraks`
--
ALTER TABLE `kontraks`
ADD CONSTRAINT `kontraks_id_perjanjian_foreign` FOREIGN KEY (`id_perjanjian`) REFERENCES `perjanjians` (`id`),
ADD CONSTRAINT `kontraks_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`),
ADD CONSTRAINT `kontraks_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `lihats`
--
ALTER TABLE `lihats`
ADD CONSTRAINT `lihats_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `lihats_id_survei_foreign` FOREIGN KEY (`id_survei`) REFERENCES `surveis` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `manajemens`
--
ALTER TABLE `manajemens`
ADD CONSTRAINT `manajemens_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `manajemens_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permintaan`
--
ALTER TABLE `permintaan`
ADD CONSTRAINT `permintaan_id_ruangan_foreign` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangans` (`id`),
ADD CONSTRAINT `permintaan_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksis` (`id`);

--
-- Ketidakleluasaan untuk tabel `ruangans`
--
ALTER TABLE `ruangans`
ADD CONSTRAINT `ruangans_id_gedung_foreign` FOREIGN KEY (`id_gedung`) REFERENCES `gedungs` (`id`);

--
-- Ketidakleluasaan untuk tabel `surveis`
--
ALTER TABLE `surveis`
ADD CONSTRAINT `surveis_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
ADD CONSTRAINT `transaksis_id_kontrak_foreign` FOREIGN KEY (`id_kontrak`) REFERENCES `kontraks` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
